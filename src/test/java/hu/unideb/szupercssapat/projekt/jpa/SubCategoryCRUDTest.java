package hu.unideb.szupercssapat.projekt.jpa;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

import javax.persistence.EntityManager;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

import hu.unideb.szupercsapat.projekt.jpa.CategoryCRUD;
import hu.unideb.szupercsapat.projekt.jpa.SubCategoryCRUD;
import hu.unideb.szupercsapat.projekt.model.products.Category;
import hu.unideb.szupercsapat.projekt.model.products.SubCategory;

@Test( singleThreaded = true )
public class SubCategoryCRUDTest {

	private static SubCategory kaja;
	private static SubCategory jatek;
	private static SubCategory ketrec;
	private static SubCategory egyeb;
	
	private static EntityManager em = CategoryCRUD.getEntityManager();
	
	//@Test(groups = {"integration-tests" } )
	//@BeforeClass
	@BeforeGroups( "SubCategoryCRUDTest")
	public void initialize()	{
		
		kaja = SubCategoryCRUD.create("kaja");
		
		jatek = SubCategoryCRUD.create("jatek");
		
		ketrec = SubCategoryCRUD.create("ketrec");
		
		egyeb = SubCategoryCRUD.create("egyeb");
		
		
		assertNotNull(kaja);
		
		assertNotNull(jatek);
		
		assertNotNull(ketrec);
		
		assertNotNull(egyeb);
		
		
		assertNotNull(kaja.getId());
		
		assertNotNull(jatek.getId());
		
		assertNotNull(ketrec.getId());
		
		assertNotNull(egyeb.getId());
		
		
		assertEquals( "kaja", kaja.getName());
		
		assertEquals( "jatek", jatek.getName());
		
		assertEquals( "ketrec", ketrec.getName());
		
		assertEquals( "egyeb", egyeb.getName());
		
	}
	
	@Test(
			groups = {"integration-tests", "SubCategoryCRUDTest" }
	)
	public void create()	{
		
		SubCategory subCategory = SubCategoryCRUD.create("subcategory");
		
		assertNotNull(subCategory);
		
		assertNotNull(subCategory.getId());
		
		assertEquals( "subcategory", subCategory.getName() );
	}
	
	@Test(
			groups = {"integration-tests", "SubCategoryCRUDTest" }
	)
	public void retreiveByName()	{
		
		SubCategory kajesz =  SubCategoryCRUD.retrieve("kaja");
		
		assertNotNull(kajesz);
		
		assertNotNull(kajesz.getId());
		
		assertEquals( "kaja", kajesz.getName() );
	}
	
	@Test(
			groups = {"integration-tests", "SubCategoryCRUDTest" }
	)
	public void retreiveByID()	{
		
		SubCategory kajesz =  SubCategoryCRUD.retrieve(kaja.getId());
		
		assertNotNull(kajesz);
		
		assertNotNull(kajesz.getId());
		
		assertEquals( "kaja", kajesz.getName() );
	}
	
	@Test(dependsOnMethods = { "create", "retreiveByName" },
			groups = {"integration-tests", "SubCategoryCRUDTest" }
	)
	public void update()	{
		
		SubCategory updateable =  SubCategoryCRUD.create("updateable");
		
		SubCategory updated = SubCategoryCRUD.update(updateable.getId(), "updated");
		
		assertNotNull( updated );
		
		assertEquals( updateable.getId(), updated.getId());
		
		assertEquals( "updated", updated.getName() );
	}
	
	@Test( dependsOnMethods = { "update" },
			groups = {"integration-tests", "SubCategoryCRUDTest" }
	) 
	public void deleteById()	{
		
		SubCategory updated = SubCategoryCRUD.retrieve("updated");
		
		SubCategoryCRUD.delete(updated.getId());
		
		SubCategory deleted = SubCategoryCRUD.retrieve("updated");
		
		assertNull(deleted);
	}
	
	@Test( 
			groups = {"integration-tests", "SubCategoryCRUDTest" }
	) 
	public void deleteByObjectRef()	{
		
		SubCategoryCRUD.delete(egyeb);
		
		SubCategory egyebUjra = SubCategoryCRUD.retrieve("egyeb");
		
		assertNull(egyebUjra);
		
	}
	
	//@AfterClass
	@AfterGroups( "SubCategoryCRUDTest")
	public static void teardown()	{
		
		for(long index = 0; index < 100L; index++ )	{
			SubCategoryCRUD.delete(index);
		}
	}
}
