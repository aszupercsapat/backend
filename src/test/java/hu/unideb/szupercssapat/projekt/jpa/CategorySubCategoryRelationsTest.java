package hu.unideb.szupercssapat.projekt.jpa;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.assertNotNull;

import java.util.Set;

import javax.persistence.EntityManager;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

import hu.unideb.szupercsapat.projekt.jpa.CategoryCRUD;
import hu.unideb.szupercsapat.projekt.jpa.JPAUtils;
import hu.unideb.szupercsapat.projekt.jpa.SubCategoryCRUD;
import hu.unideb.szupercsapat.projekt.model.products.Category;
import hu.unideb.szupercsapat.projekt.model.products.SubCategory;

@Test( singleThreaded = true )
public class CategorySubCategoryRelationsTest {
	
	private static Category kutya1;
	private static Category cica;
	private static Category horcsog;
	private static Category vipera;
	
	private static SubCategory kaja;
	private static SubCategory jatek;
	private static SubCategory ketrec;
	private static SubCategory egyeb;

	//@BeforeClass
	@BeforeGroups("CategorySubCategoryRelationsTest")
	public static void initialize() throws InterruptedException {
		
		kutya1 = CategoryCRUD.create("kutya");
		
		cica = CategoryCRUD.create("cica");
		
		horcsog = CategoryCRUD.create("hörcsög");
		
		vipera = CategoryCRUD.create("vipera");
		
		
		kaja = SubCategoryCRUD.create("kaja");
		
		jatek = SubCategoryCRUD.create("jatek");
		
		ketrec = SubCategoryCRUD.create("ketrec");
		
		egyeb = SubCategoryCRUD.create("egyeb");
		
	}
	
	@Test(groups = {"integration-tests", "CategorySubCategoryRelationsTest" },
			dependsOnGroups = { "CategoryCRUDTest", "SubCategoryCRUDTest" }
			)
	public static void checkCreation()	{
		assertNotNull(kutya1);
		assertNotNull(cica);
		assertNotNull(horcsog);
		assertNotNull(vipera);
		
		assertNotNull(kaja);
		assertNotNull(jatek);
		assertNotNull(ketrec);
		assertNotNull(egyeb);
		
		EntityManager em = CategoryCRUD.getEntityManager();
		
		assertFalse(JPAUtils.isDetached(em, kutya1));
		assertFalse(JPAUtils.isDetached(em, cica));
		assertFalse(JPAUtils.isDetached(em, horcsog));
		assertFalse(JPAUtils.isDetached(em, vipera));
		
		assertFalse(JPAUtils.isDetached(em, kaja));
		assertFalse(JPAUtils.isDetached(em, jatek));
		assertFalse(JPAUtils.isDetached(em, ketrec));
		assertFalse(JPAUtils.isDetached(em, egyeb));
		
	}
	
	@Test(groups = {"integration-tests", "CategorySubCategoryRelationsTest" },
		dependsOnMethods = { "checkCreation" },
		dependsOnGroups = { "CategoryCRUDTest", "SubCategoryCRUDTest" }
		
			)
	public void addingSubCategoryToCategory()	{
		
		// to make kutya1 persistent again
		kutya1 = CategoryCRUD.retrieve(kutya1.getId());
		
		assertNotNull(kutya1);
		
		assertNotNull(kutya1.getId());
		
		assertNotNull(kaja);
		
		CategoryCRUD.addSubCategoryToCategory(kutya1, kaja);
		
		assertNotNull(jatek);
		
		CategoryCRUD.addSubCategoryToCategory(kutya1, jatek);
		
		Set<SubCategory> subCategories = CategoryCRUD.listSubCategoriesByCategory(kutya1);
		
		assertNotNull(subCategories);
		
		assertEquals(Integer.valueOf(2), Integer.valueOf(subCategories.size() ) );
		
		assertTrue(subCategories.contains(kaja) );
		assertTrue(subCategories.contains(jatek) );
		
	}
	
	//@AfterClass
	//@AfterGroups("CategorySubCategoryRelationsTest")
	public static void teardown()	{
		
		for(long index = 0; index < 100L; index++ )	{
			SubCategoryCRUD.delete(index);
			CategoryCRUD.delete(index);
		}
	}
}
