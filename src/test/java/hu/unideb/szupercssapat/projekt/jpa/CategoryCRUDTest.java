package hu.unideb.szupercssapat.projekt.jpa;


import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.transaction.SystemException;

import org.junit.Ignore;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;
import static org.testng.AssertJUnit.*;

import hu.unideb.szupercsapat.projekt.jpa.CategoryCRUD;
import hu.unideb.szupercsapat.projekt.jpa.EntityManagerUtil;
import hu.unideb.szupercsapat.projekt.model.products.Category;

@Test( singleThreaded = true )
public class CategoryCRUDTest {
	
	private static Category kutya1;
	private static Category cica;
	private static Category horcsog;
	private static Category vipera;
	
	private static EntityManager em = CategoryCRUD.getEntityManager();


	//@Test(groups = {"integration-tests" })
	//@BeforeClass
	@BeforeGroups("CategoryCRUDTest")
	public static void initialize() throws InterruptedException {
		
		kutya1 = CategoryCRUD.create("kutya");
		
		cica = CategoryCRUD.create("cica");
		
		horcsog = CategoryCRUD.create("hörcsög");
		
		vipera = CategoryCRUD.create("vipera");
		
		assertNotNull(kutya1);
		
		assertNotNull(cica);
		
		assertNotNull(horcsog);
		
		assertNotNull(vipera);
		
		
		assertNotNull(kutya1.getId());
		
		assertNotNull(cica.getId());
		
		assertNotNull(horcsog.getId());
		
		assertNotNull(vipera.getId());
		
		assertNotNull( CategoryCRUD.retrieve("kutya") );
		
		assertNotNull( CategoryCRUD.retrieve("cica") );
		
		assertNotNull( CategoryCRUD.retrieve("hörcsög") );
		
		assertNotNull( CategoryCRUD.retrieve("vipera") );
		
		
		assertNotNull( CategoryCRUD.retrieve(kutya1.getId()) );
		
		assertNotNull( CategoryCRUD.retrieve(cica.getId()) );
		
		assertNotNull( CategoryCRUD.retrieve(horcsog.getId()) );
		
		assertNotNull( CategoryCRUD.retrieve(vipera.getId()) );
		
	}
	
	@Test( /* dependsOnMethods = { "initialize" } */ groups = {"integration-tests", "CategoryCRUDTest" } )
	public void createLo() {
		
		EntityManager em = EntityManagerUtil.getEntityManager();
		
		Category lovacska = CategoryCRUD.create("lovacska");
		
		assertNotNull(lovacska);
		
		assertEquals("lovacska", lovacska.getName() );
		
		assertNotNull(lovacska.getId());
		
		assertNotNull( em.find(Category.class, lovacska.getId()) );
		
		lovacska = em.find(Category.class, lovacska.getId());
		
		em.remove(lovacska);
		
		em.close();
		
		
	}
	
	@Test( /* dependsOnMethods = { "initialize" } */
			groups = {"integration-tests", "CategoryCRUDTest" }
	)
	public void retreiveLoWithQuery()	{
		
		Category lovacska = CategoryCRUD.create("lovacska");
		
		assertNotNull(lovacska);
		
		assertEquals("lovacska", lovacska.getName() );
		
		Category category = null;
		
		EntityTransaction tx = em.getTransaction();
		
		try	{
			tx.begin();
			//em.joinTransaction();
			
			TypedQuery<Category> query = em.createQuery("from Category c where c.name = :name", Category.class);
	        query.setParameter("name", "lovacska");
	        category = query.getSingleResult() ;
	        
	        tx.commit();
		}
		catch(NoResultException ex)	{
			category = null;
			tx.rollback();
		}
		catch(Throwable ex)	{
			// Any other bad thing happens
			tx.rollback();
		}
		
		assertNotNull(category);
		
		assertEquals(  "lovacska" , category.getName());
	}

	@Test( /* dependsOnMethods = { "initialize" } */
			groups = {"integration-tests", "CategoryCRUDTest" }
	)
	public void createKutya() 	{
		Category kutyus1 = CategoryCRUD.create("kutya");
		
		Category kutya2 = CategoryCRUD.retrieve("kutya");
		
		assertNotNull(kutya2);
		
		assertEquals( "kutya" , kutya2.getName() );
		
		assertEquals( kutyus1.getName(), kutya2.getName());
		
		assertEquals(kutyus1.getId(), kutya2.getId());
		
		//assertSame(kutyus1, kutya2);
		// már minden tranzakcióhoz külön entitymanager lesz
	}
	
	@Test( /* dependsOnMethods = { "initialize" } */
			groups = {"integration-tests", "CategoryCRUDTest" } 
	)
	public void retreiveCica()	{
		
		assertNotNull(cica);
		
		Category cicus = CategoryCRUD.retrieve("cica");
		
		assertNotNull(cicus);
		
		assertEquals( "cica" , cicus.getName() );
		
		assertEquals( cica.getId(),  cicus.getId());
	}
	
	@Test( /* dependsOnMethods = { "initialize" } */
			groups = {"integration-tests", "CategoryCRUDTest" }
	)
	public void nonexistent()	{
		Category halacka = CategoryCRUD.retrieve("halacka");
		
		assertNull(halacka);
	}
	
	@Test( /* dependsOnMethods = { "initialize" } */
			groups = {"integration-tests", "CategoryCRUDTest" }
	)
	public void retreiveById()	{
		
		assertNotNull(kutya1);
		
		Category kutyus = CategoryCRUD.retrieve(kutya1.getId());
		
		assertNotNull(kutyus);
		
		assertEquals( kutya1.getName(), kutyus.getName());
		
	}
	
	@Test( enabled = false, /* dependsOnMethods = { "initialize" } */
			groups = {"integration-tests", "CategoryCRUDTest" }
	)
	public void retrieveWithEntityManager()	{
		
		//kutya1 = CategoryCRUD.create("kutya");
		
		assertNotNull(kutya1);
		
		assertNotNull(kutya1.getId());
		
		Category kutyus = em.find(Category.class, kutya1.getId() );
		
		assertNotNull(kutyus);
		
		assertEquals( kutya1.getName(), kutyus.getName());
		
	}
	
	@Test( dependsOnMethods = {  /*"initialize", */ /*"retrieveWithEntityManager" ,*/ "retreiveById", "createKutya"  },
			groups = {"integration-tests", "CategoryCRUDTest" }
	)
	public void updateKutya()	{
		
		assertNotNull(kutya1);
		
		//Category kutyuska = CategoryCRUD.retrieve(kutya1.getId());
		
		Category kutyuska = CategoryCRUD.create("kutya");
		
		assertNotNull(kutyuska);
		
		kutyuska = CategoryCRUD.update(kutyuska.getId(), "kutyuska");
		
		assertNull( CategoryCRUD.retrieve("kutya") );
		
		assertNotNull(  CategoryCRUD.retrieve("kutyuska") );
		
	}
	
	// skip this test coz it gives error when runnign all tests, but passing when we run tests only in this class
	@Test( dependsOnMethods = { "updateKutya" },
			groups = {"integration-tests", "CategoryCRUDTest" }
	)
	public void deleteKutyuska()	{
		
		Category kutyuska = CategoryCRUD.retrieve("kutyuska");
		
		assertNotNull(kutyuska);
		
		CategoryCRUD.delete(kutyuska.getId());
		
		kutyuska = CategoryCRUD.retrieve("kutyuska");
		
		assertNull(kutyuska);
		
	}
	
	//@AfterClass
	@AfterGroups("CategoryCRUDTest")
	public static void teardown()	{
		
		for(long index = 0; index < 100L; index++ )	{
			CategoryCRUD.delete(index);
		}
	}
	
}
