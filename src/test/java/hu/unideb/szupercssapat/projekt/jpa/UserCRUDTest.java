package hu.unideb.szupercssapat.projekt.jpa;

import static org.testng.AssertJUnit.*;

import java.security.NoSuchAlgorithmException;

import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

import hu.unideb.szupercsapat.projekt.jpa.HashService;
import hu.unideb.szupercsapat.projekt.jpa.ProductCRUD;
import hu.unideb.szupercsapat.projekt.jpa.UserCRUD;
import hu.unideb.szupercsapat.projekt.model.address.HunAddress;
import hu.unideb.szupercsapat.projekt.model.user.User;
import hu.unideb.szupercsapat.projekt.model.user.UserAccessRights;
import hu.unideb.szupercsapat.projekt.model.user.UserData;

@Test( singleThreaded = true )
public class UserCRUDTest {

	
	private static User user1;
	private static User user2;
	private static User user3;
	
	private static User localUser;

	@BeforeGroups("UserCRUDTest")
	public static void initialize() throws NoSuchAlgorithmException	{
		
		HunAddress homeAddress1 = new HunAddress(3000, "Ózd", "Váci", "Utca", "22/A", 2, "4B");
		UserData user1Data = new UserData("Majka", "Papa", "+36306666666", homeAddress1);
		user1 = new User("valaki.aki@akarmi.boom", UserAccessRights.CUSTOMER, "1234", user1Data);
		user1 = UserCRUD.create(user1);
		
		HunAddress homeAddress2 = new HunAddress(4000, "Eger", "Bor", "Út", "10", 5, "2");
		UserData user2Data = new UserData("Borász", "Sándor", "+36706666666", homeAddress2);
		user2 = new User("chick536@something.com", UserAccessRights.CUSTOMER, "riot", user2Data);
		user2 = UserCRUD.create(user2);
		
		HunAddress homeAddress3 = new HunAddress(5000, "BudaPest", "Erzsébet", "Utca", "10", 1, null);
		UserData user3Data = new UserData("Black", "Rachel", "+36206666666", homeAddress3);
		user3 = new User("rachel.black@thingsmatter.com", UserAccessRights.CUSTOMER, "anything", user3Data);
		user3 = UserCRUD.create(user3);
	}

	
	
	@Test( groups = {"integration-tests", "UserCRUDTest" } )
	public void retreive()	{
		
		assertNotNull(user1);
		
		User user1Again = UserCRUD.retreive(user1.getId());
		
		assertNotNull(user1Again);
		
	}
	
	@Test(groups = {"integration-tests", "UserCRUDTest" })
	public void createWithCRUD() throws NoSuchAlgorithmException	{
		
		HunAddress homeAddress = new HunAddress(4040, "Debrecen", "Piac", "Utca", "10", null, null);
		UserData userData = new UserData("Pablo", "Escobar", "+36203333333", homeAddress);
		User userr = new User("pablo.escobar@badguy.com", UserAccessRights.CUSTOMER, "anything", userData);
		localUser = UserCRUD.create(userr);
		
		assertNotNull(localUser.getId());
		
		assertNotNull(localUser.getEmailAddress());
		
		assertNotNull(localUser.getHashedPassword());
		
		assertEquals("anything", localUser.getTransientPassword());
		
	}
	
	@Test(groups = {"integration-tests", "UserCRUDTest" },
			dependsOnMethods = { "createWithCRUD" } )
	public void update() throws NoSuchAlgorithmException	{
		
		Long id = localUser.getId();
		
		User transientUser = new User("newmail@something.com", UserAccessRights.APPLICATION_ADMIN, "idococaine");
		
		localUser.setEmailAddress(transientUser.getEmailAddress());
		
		localUser.setRights(transientUser.getRights());
		
		localUser.setTransientPassword(transientUser.getTransientPassword());
		
		assertEquals("idococaine", localUser.getTransientPassword());
		
		localUser = UserCRUD.update(localUser);
		
		assertEquals( Long.valueOf(id),  localUser.getId());
		
		assertEquals( "newmail@something.com",  localUser.getEmailAddress());
		
		assertEquals( UserAccessRights.APPLICATION_ADMIN,  localUser.getRights() );
		
		//assertEquals( "idococaine", localUser.getTransientPassword());
		// transient fields get deleted after JPA operations
		
		
		
		String hashed = HashService.getSecurePassword("idococaine", "newmail@something.com");
		
		assertEquals( hashed,  localUser.getHashedPassword() );
	}
	
	@AfterGroups("UserCRUDTest")
	public static void teardown()	{
		
		for(long index = 0; index < 100L; index++ )	{
			UserCRUD.delete(index);
		}
		
		
	}
	
}
