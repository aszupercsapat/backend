package hu.unideb.szupercssapat.projekt.jpa;

import static org.testng.AssertJUnit.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

import hu.unideb.szupercsapat.projekt.jpa.CategoryCRUD;
import hu.unideb.szupercsapat.projekt.jpa.ProductCRUD;
import hu.unideb.szupercsapat.projekt.jpa.SubCategoryCRUD;
import hu.unideb.szupercsapat.projekt.model.products.Category;
import hu.unideb.szupercsapat.projekt.model.products.Product;
import hu.unideb.szupercsapat.projekt.model.products.ProductDescription;
import hu.unideb.szupercsapat.projekt.model.products.SubCategory;

@Test( singleThreaded = true )
public class ProductCRUDTest {
		
	private static Category kutya1;
	private static Category cica;
	private static Category horcsog;
	private static Category vipera;
	
	private static SubCategory kaja;
	private static SubCategory jatek;
	private static SubCategory ketrec;
	private static SubCategory egyeb;

	private static Product macskaKajesz = null;
	private static Product kutyaKajesz = null;
	
	//@BeforeClass
	@BeforeGroups("ProductCRUDTest")
	public static void initialize() throws InterruptedException {
		
		kutya1 = CategoryCRUD.create("kutya");
		
		cica = CategoryCRUD.create("cica");
		
		horcsog = CategoryCRUD.create("hörcsög");
		
		vipera = CategoryCRUD.create("vipera");
		
		
		kaja = SubCategoryCRUD.create("kaja");
		
		jatek = SubCategoryCRUD.create("jatek");
		
		ketrec = SubCategoryCRUD.create("ketrec");
		
		egyeb = SubCategoryCRUD.create("egyeb");
		
	}
	
	@Test( groups = {"integration-tests", "ProductCRUDTest" },
			dependsOnGroups="CategorySubCategoryRelationsTest")
	public void createWithTransientProduct()	{
		
		ProductDescription pd = new ProductDescription();
		pd.setManufacturer("Purina");
		pd.setName("Száraz táp macskáknak");
		pd.setPriceInHUF(400L);
		pd.setShortDescription("legjobb izé hozé finomság, ketté áll a mcsakád füle tőle");
		
		Product macskaTap = new Product();
		macskaTap.setProductDescription(pd);
		macskaTap.setCategory(cica);
		macskaTap.setSubCategory(kaja);
		
		macskaTap = ProductCRUD.create(macskaTap);
		
		assertNotNull(macskaTap.getId());
		
		assertNotNull(macskaTap);
		
		macskaKajesz = macskaTap;
		
		assertNotNull(macskaKajesz);
		
		assertNotNull(macskaKajesz.getId());
		
		//macskaKajesz = null;
		
	}
	
	@Test( groups = {"integration-tests", "ProductCRUDTest" },
			dependsOnGroups="CategorySubCategoryRelationsTest"	)
	public void crateWithConstructorFields()	{
		
		kutyaKajesz = ProductCRUD.create("Pedigree", "Dog food for your best friend", 600L, "legjobb imádja a kutyád veddmeg!", kutya1, kaja);

		assertNotNull(kutyaKajesz.getId());
		
		assertNotNull(kutyaKajesz.getCategory() );
		
		//kutyaKajesz = null;
		
		assertNotNull(ProductCRUD.retreive(kutyaKajesz.getId()));
	}
	
	@Test( groups = {"integration-tests", "ProductCRUDTest" },	
			dependsOnMethods = { "createWithTransientProduct"  },
			dependsOnGroups="CategorySubCategoryRelationsTest"
			)
	public void retreiveByIdTest()	{
		
		assertNotNull(macskaKajesz);
		
		assertNotNull(macskaKajesz.getId());
		
		Product cicaKaja = ProductCRUD.retreive(macskaKajesz.getId());
		
		assertNotNull(cicaKaja);
		
		
		assertEquals( "Purina", cicaKaja.getProductDescription().getManufacturer());
		
		assertEquals( "Száraz táp macskáknak", cicaKaja.getProductDescription().getName() );
		
		assertEquals( Long.valueOf(400L), cicaKaja.getProductDescription().getPriceInHUF());
		
		assertEquals( "legjobb izé hozé finomság, ketté áll a mcsakád füle tőle", cicaKaja.getProductDescription().getShortDescription());
		
		Category catCategory = CategoryCRUD.retrieve(cicaKaja.getCategory().getId() );
		
		assertEquals( "cica", cicaKaja.getCategory().getName() );
		
		assertEquals( "cica", catCategory.getName() );
		
		SubCategory subCategoryFood = SubCategoryCRUD.retrieve( cicaKaja.getSubCategory().getId() );
		
		assertEquals( "kaja", cicaKaja.getSubCategory().getName() );
		
		assertEquals( "kaja", subCategoryFood.getName() );
		
		
	}
	
	@Test( groups = {"integration-tests", "ProductCRUDTest" }	,
			dependsOnMethods = { "createWithTransientProduct" , "crateWithConstructorFields"  },
			dependsOnGroups="CategorySubCategoryRelationsTest"
			)
	public void listProductById1()	{
		
		// 1 db kutya kaja 
		List<Product> products = ProductCRUD.retreiveByCategoryIdAndSubCategoryId(
				kutya1.getId(), kaja.getId(), 0, 5);
		
		
		assertNotNull(products);
		
		assertEquals( new Integer(1) , Integer.valueOf(products.size()) );
		
	}
	
	@Test( groups = {"integration-tests", "ProductCRUDTest" }	,
			dependsOnMethods = { "createWithTransientProduct" , "crateWithConstructorFields"  },
			dependsOnGroups="CategorySubCategoryRelationsTest"
			)
	public void listProductById2()	{
		
		// 1 db kutya kaja 
		List<Product> products = ProductCRUD.retreiveByCategoryIdAndSubCategoryId(
				cica.getId(), kaja.getId(), 0, 5);
		
		
		assertNotNull(products);
		
		assertEquals( new Integer(1) , Integer.valueOf(products.size()) );
		
	}
	
	@Test( groups = {"integration-tests", "ProductCRUDTest" }	,
			dependsOnMethods = { "createWithTransientProduct" , "crateWithConstructorFields"  },
			dependsOnGroups="CategorySubCategoryRelationsTest"
			)
	public void retreiveByCategoryId1()	{
		List<Product> products = ProductCRUD.retreiveByCategoryId(
				cica.getId(),  0, 5);
		
		assertNotNull(products);
		
		assertEquals( new Integer(1) , Integer.valueOf(products.size()) );
	}
	
	@Test( groups = {"integration-tests", "ProductCRUDTest" }	,
			dependsOnMethods = { "createWithTransientProduct" , "crateWithConstructorFields"  },
			dependsOnGroups="CategorySubCategoryRelationsTest"
			)
	public void retreiveAnyProducts()	{
		List<Product> products = ProductCRUD.retreiveProductsOfAnyKind( 0, 5);
		
		assertNotNull(products);
		
		assertEquals( new Integer(2) , Integer.valueOf(products.size()) );
	}
	
	
	
	//@AfterClass
	//@AfterGroups("ProductCRUDTest")
	public static void teardown()	{
		
		
		
		for(long index = 0; index < 100L; index++ )	{
			ProductCRUD.delete(index);
		}
		
		for(long index = 0; index < 100L; index++ )	{
			SubCategoryCRUD.delete(index);
		}
		
		for(long index = 0; index < 100L; index++ )	{
			CategoryCRUD.delete(index);
		}
	}
}
