package hu.unideb.szupercssapat.projekt.jpa;

import static org.testng.Assert.assertNotNull;

import java.security.NoSuchAlgorithmException;

import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

import hu.unideb.szupercsapat.projekt.jpa.CategoryCRUD;
import hu.unideb.szupercsapat.projekt.jpa.OrderCRUD;
import hu.unideb.szupercsapat.projekt.jpa.OrderedProductCRUD;
import hu.unideb.szupercsapat.projekt.jpa.ProductCRUD;
import hu.unideb.szupercsapat.projekt.jpa.SubCategoryCRUD;
import hu.unideb.szupercsapat.projekt.jpa.UserCRUD;
import hu.unideb.szupercsapat.projekt.model.address.HunAddress;
import hu.unideb.szupercsapat.projekt.model.order.Order;
import hu.unideb.szupercsapat.projekt.model.order.OrderedProduct;
import hu.unideb.szupercsapat.projekt.model.products.Category;
import hu.unideb.szupercsapat.projekt.model.products.Product;
import hu.unideb.szupercsapat.projekt.model.products.ProductDescription;
import hu.unideb.szupercsapat.projekt.model.products.SubCategory;
import hu.unideb.szupercsapat.projekt.model.user.User;
import hu.unideb.szupercsapat.projekt.model.user.UserAccessRights;
import hu.unideb.szupercsapat.projekt.model.user.UserData;

public class OrderCRUDTest {

	private static Category kutya1;
	private static Category cica;
	private static SubCategory kaja;
	private static SubCategory jatek;
	private static Product macskaTapProduct1;
	private static OrderedProduct orderedProduct1;
	private static Product macskaTapProduct2;
	private static User user1;
	private static User user2;
	private static OrderedProduct orderedProduct2;


	@BeforeGroups("OrderCRUDTest")
	public static void initialize() throws NoSuchAlgorithmException	{
		
		kutya1 = CategoryCRUD.create("kutya");
		
		cica = CategoryCRUD.create("cica");
		
		
		kaja = SubCategoryCRUD.create("kaja");
		
		jatek = SubCategoryCRUD.create("jatek");
		
		
		ProductDescription pd1 = new ProductDescription();
		pd1.setManufacturer("Purina");
		pd1.setName("Száraz táp macskáknak");
		pd1.setPriceInHUF(400L);
		pd1.setShortDescription("Best száraz");
		
		ProductDescription pd2 = new ProductDescription();
		pd2.setManufacturer("Whiskas");
		pd2.setName("Vadhús");
		pd2.setPriceInHUF(400L);
		pd2.setShortDescription("Best vadhús"
				+ "");
		
		macskaTapProduct1 = new Product();
		macskaTapProduct1.setProductDescription(pd1);
		macskaTapProduct1.setCategory(cica);
		macskaTapProduct1.setSubCategory(kaja);
		
		macskaTapProduct2 = new Product();
		macskaTapProduct2.setProductDescription(pd2);
		macskaTapProduct2.setCategory(cica);
		macskaTapProduct2.setSubCategory(kaja);
		
		macskaTapProduct1 = ProductCRUD.create(macskaTapProduct1);
		
		macskaTapProduct2 = ProductCRUD.create(macskaTapProduct2);
		
		HunAddress homeAddress1 = new HunAddress(3000, "Ózd", "Váci", "Utca", "22/A", 2, "4B");
		UserData user1Data = new UserData("Majka", "Papa", "+36306666666", homeAddress1);
		user1 = new User("valaki.aki@akarmi.boom", UserAccessRights.CUSTOMER, "1234", user1Data);
		user1 = UserCRUD.create(user1);
		
		HunAddress homeAddress2 = new HunAddress(4000, "Eger", "Bor", "Út", "10", 5, "2");
		UserData user2Data = new UserData("Borász", "Sándor", "+36706666666", homeAddress2);
		user2 = new User("chick536@something.com", UserAccessRights.CUSTOMER, "riot", user2Data);
		user2 = UserCRUD.create(user2);
		
		Order order1 = new Order(user1, user1.getUserData().getAddress(), user1.getUserData().getAddress());
		
		order1 = OrderCRUD.create(order1);
		
		orderedProduct1 = new OrderedProduct( 1L, macskaTapProduct1, order1);
		
		orderedProduct2 = new OrderedProduct( 5L, macskaTapProduct2, order1);
		
		orderedProduct1 = OrderedProductCRUD.create(orderedProduct1);
		
		orderedProduct2 = OrderedProductCRUD.create(orderedProduct2);
		
	}
	
	@Test( groups = { "OrderCRUDTest", "integration-tests" },
			dependsOnGroups="OrderedProductCRUDTest"
			)
	public void createWithObject()	{
		
		assertNotNull(orderedProduct1);
		
		assertNotNull(orderedProduct1.getId());
		
		assertNotNull(orderedProduct2);
		
		assertNotNull(orderedProduct2.getId());
		
	}
	
	/*
	@AfterGroups("OrderCRUDTest")
	public void teardown()	{
		
		for(long index = 0; index < 100L; index++)	{
			OrderedProductCRUD.delete(index);
		}
	}
	*/
}
