package hu.unideb.szupercssapat.projekt.jpa;

import hu.unideb.szupercsapat.projekt.jpa.HashService;
import org.testng.annotations.Test;

import java.security.NoSuchAlgorithmException;

import static org.testng.AssertJUnit.*;

public class HashServiceTest {
	
    @Test(groups = { "hashservice-test" })
    public void getSecurePassword() throws NoSuchAlgorithmException {
        String password = "Jelszó";
        String email = "mrcsempe@gmail.com";

        String hash1 = HashService.getSecurePassword(password, email);
        String hash2 = HashService.getSecurePassword(password, email);

        System.out.println(password + " != " + hash1);
        assertFalse(password.equals(hash1));

        System.out.println(password + email + " != " + hash1);
        assertFalse(password.equals(hash1));

        System.out.println(hash1 + " == " + hash2);
        assertEquals(hash1, hash2);
    }
    
    @Test(groups = { "hashservice-test" })
    public void oneMoreTest() throws NoSuchAlgorithmException	{
    	String hashed1 = HashService.getSecurePassword("idococaine", "newmail@something.com");
    	
    	String hashed2 = HashService.getSecurePassword("idococaine", "newmail@something.com");
    	
    	assertEquals(hashed1, hashed2);
    }
    
}
