package hu.unideb.szupercssapat.projekt.jpa;

import static org.testng.Assert.assertNotNull;

import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

import hu.unideb.szupercsapat.projekt.jpa.CategoryCRUD;
import hu.unideb.szupercsapat.projekt.jpa.OrderedProductCRUD;
import hu.unideb.szupercsapat.projekt.jpa.ProductCRUD;
import hu.unideb.szupercsapat.projekt.jpa.SubCategoryCRUD;
import hu.unideb.szupercsapat.projekt.model.order.OrderedProduct;
import hu.unideb.szupercsapat.projekt.model.products.Category;
import hu.unideb.szupercsapat.projekt.model.products.Product;
import hu.unideb.szupercsapat.projekt.model.products.ProductDescription;
import hu.unideb.szupercsapat.projekt.model.products.SubCategory;

public class OrderedProductCRUDTest {
	
	
	private static Category kutya1;
	private static Category cica;
	private static SubCategory kaja;
	private static SubCategory jatek;
	
	private static OrderedProduct orderedProduct1;
	private static Product macskaTapProduct;

	@BeforeGroups("OrderedProductCRUDTest")
	public static void initialize()	{
		
		kutya1 = CategoryCRUD.create("kutya");
		
		cica = CategoryCRUD.create("cica");
		
		
		kaja = SubCategoryCRUD.create("kaja");
		
		jatek = SubCategoryCRUD.create("jatek");
		
		
		ProductDescription pd1 = new ProductDescription();
		pd1.setManufacturer("Purina");
		pd1.setName("Száraz táp macskáknak");
		pd1.setPriceInHUF(400L);
		pd1.setShortDescription("legjobb izé hozé finomság, ketté áll a mcsakád füle tőle");
		
		macskaTapProduct = new Product();
		macskaTapProduct.setProductDescription(pd1);
		macskaTapProduct.setCategory(cica);
		macskaTapProduct.setSubCategory(kaja);
		
		macskaTapProduct = ProductCRUD.create(macskaTapProduct);
		
		orderedProduct1 = new OrderedProduct( 1L, macskaTapProduct, null);
		
		orderedProduct1 = OrderedProductCRUD.create(orderedProduct1);
		
	}
	
	@Test( groups = { "OrderedProductCRUDTest", "integration-tests" },
			dependsOnGroups="ProductCRUDTest"
			)
	public void createWithObject()	{
		
		assertNotNull(orderedProduct1);
		
		assertNotNull(orderedProduct1.getId());
		
	}
	
	@Test( groups = { "OrderedProductCRUDTest", "integration-tests" },
			dependsOnGroups="ProductCRUDTest"
			)
	public void createWithArguments()	{
		
		ProductDescription pd2 = new ProductDescription();
		pd2.setManufacturer("Whiskas");
		pd2.setName("HusiMusi Cicusnak");
		pd2.setPriceInHUF(500L);
		pd2.setShortDescription("MIJAÚÚÚ");
		
		Product macskaTapProduct2 = new Product();
		macskaTapProduct2.setProductDescription(pd2);
		macskaTapProduct2.setCategory(cica);
		macskaTapProduct2.setSubCategory(kaja);
		
		macskaTapProduct2 = ProductCRUD.create(macskaTapProduct2); // without this JPA cannot save foreign key so this is needed
																	// if we leave out this line, but add Cascade PERSIST it still fails we need this oersisted first then we can persist OrderedProduct
		
		OrderedProduct op = OrderedProductCRUD.create(5L, macskaTapProduct2, null);
		
		assertNotNull(op);
		
		assertNotNull(op.getId());
		
	}
	
	@Test( groups = { "OrderedProductCRUDTest", "integration-tests" },
			dependsOnGroups="ProductCRUDTest"
			)
	public void retreive()	{
		
		assertNotNull(orderedProduct1);
		
		assertNotNull(orderedProduct1.getId());
		
		OrderedProduct retreived = OrderedProductCRUD.retreive(orderedProduct1.getId());
		
		assertNotNull(retreived);
		
	}
	
	/*
	@AfterGroups("OrderedProductCRUDTest")
	public void teardown()	{
		
		for(long index = 0; index < 100L; index++)	{
			
			ProductCRUD.delete(index);
			OrderedProductCRUD.delete(index);
			SubCategoryCRUD.delete(index);
			CategoryCRUD.delete(index);
			
		}
	}*/
}
