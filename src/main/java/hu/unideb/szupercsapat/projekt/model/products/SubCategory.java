package hu.unideb.szupercsapat.projekt.model.products;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
public class SubCategory {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@NotNull
	@Column(nullable = false, unique = true)
	private String name;
	
	// bidirectional many to one
	//@ManyToOne(fetch = FetchType.LAZY)
	
	
	// must call persist on the category part of the manytomany ex: em.persist(someCategory)
	@ManyToMany(mappedBy = "subCategories" , fetch = FetchType.LAZY )
	/*
	@JsonIdentityInfo(
			  generator = ObjectIdGenerators.PropertyGenerator.class, 
			 // generator = ObjectIdGenerators.IntSequenceGenerator.class, 
			  //generator =   ObjectIdGenerators.UUIDGenerator.class,
			  property = "id")
			  */
	@JsonBackReference
	private Set<Category> categories = new HashSet<>();

	
	
	public SubCategory() {
		super();
	}
	
	public SubCategory(Long id)	{
		this.id = id;
		this.name = null;
		this.categories = null;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Category> getCategories() {
		return categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}

	public Long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof SubCategory))
			return false;
		SubCategory other = (SubCategory) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
}
