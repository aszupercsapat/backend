package hu.unideb.szupercsapat.projekt.model.order;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.jboss.resteasy.spi.NotImplementedYetException;

import hu.unideb.szupercsapat.projekt.jpa.converters.LocalDateConverter;
import hu.unideb.szupercsapat.projekt.model.address.HunAddress;
import hu.unideb.szupercsapat.projekt.model.user.User;

@Entity
@Table(name = "orders") // order is a reserved keyword in SQL
public class Order implements Serializable {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne( fetch = FetchType.EAGER )	//
	@JoinColumn( name = "USER_ID", nullable = false)	// foreign key
	private User user;
	
	//need to override embedded attributes because of embedded name conflict
	@NotNull
	@Column(nullable = false)
	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name = "postalCode", column = @Column(name = "BILLING_POSTAL_CODE")),
		@AttributeOverride(name = "countryEnum", column = @Column(name = "BILLING_COUNTRY_ENUM")),
		@AttributeOverride(name = "city", column = @Column(name = "BILLING_CITY")),
		@AttributeOverride(name = "nameOfPublicPlace", column = @Column(name = "BILLING_NAME_OF_PUBLIC_PLACE")),
		@AttributeOverride(name = "typeOfPublicPlace", column = @Column(name = "BILLING_TYPE_OF_PUBLIC_PLACE")),
		@AttributeOverride(name = "streetNumber", column = @Column(name = "BILLING_STREET_NUMBER")),
		@AttributeOverride(name = "floorNumber", column = @Column(name = "BILLING_FLOOR_NUMBER")),
		@AttributeOverride(name = "door", column = @Column(name = "BILLING_DOOR")),
	})
	private HunAddress billingAddress;
	
	@NotNull
	@Column(nullable = false)
	private HunAddress shippingAddress;
	
	@OneToMany(mappedBy = "order", fetch = FetchType.EAGER /*, cascade = {CascadeType.PERSIST, CascadeType.MERGE } */)
	private List<OrderedProduct> orderedProducts = new ArrayList<OrderedProduct>();
	
	@NotNull
	@Column(nullable = false)
	@Convert(converter = LocalDateConverter.class) 
	private LocalDate orderDate; // java8 time
	
	public Order() {
		super();
	}

	public Order(User user, HunAddress billingAddress, HunAddress shippingAddress, 
			LocalDate orderDate) {
		super();
		this.user = user;
		this.billingAddress = billingAddress;
		this.shippingAddress = shippingAddress;
		this.orderDate = orderDate;
	}
	
	public Order(User user, HunAddress billingAddress, HunAddress shippingAddress) {
		super();
		this.user = user;
		this.billingAddress = billingAddress;
		this.shippingAddress = shippingAddress;
		this.orderDate = LocalDate.now();
	}

	public Long getActualPrice()	{
		return orderedProducts
				.stream()
				.mapToLong( (e) -> e.getProduct().getProductDescription().getPriceInHUF() )
				.sum();
		
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public HunAddress getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(HunAddress billingAddress) {
		this.billingAddress = billingAddress;
	}

	public HunAddress getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(HunAddress shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public List<OrderedProduct> getOrderedProducts() {
		return orderedProducts;
	}

	public void setOrderedProducts(List<OrderedProduct> orderedProducts) {
		this.orderedProducts = orderedProducts;
	}

	public LocalDate getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}

	public Long getId() {
		return id;
	}

	/**
	 * Updates an Order which already exists in the database.
	 * @param thisIsGoingToBeUpdated
	 * @param thisHasUpdatedDatas
	 * @return
	 */
	public static Order updateOrder(Order thisIsGoingToBeUpdated, Order thisHasUpdatedDatas)	{
		
		//TODO
		
		throw new NotImplementedYetException();
		
		//return thisIsGoingToBeUpdated;
		
	}
	
	
	/**
	 * Checks everything except id.
	 * Ignores id.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((billingAddress == null) ? 0 : billingAddress.hashCode());
		result = prime * result + ((orderDate == null) ? 0 : orderDate.hashCode());
		result = prime * result + ((orderedProducts == null) ? 0 : orderedProducts.hashCode());
		result = prime * result + ((shippingAddress == null) ? 0 : shippingAddress.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	/**
	 * Ignores id.
	 * Checks equals on everything else.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Order))
			return false;
		Order other = (Order) obj;
		if (billingAddress == null) {
			if (other.billingAddress != null)
				return false;
		} else if (!billingAddress.equals(other.billingAddress))
			return false;
		if (orderDate == null) {
			if (other.orderDate != null)
				return false;
		} else if (!orderDate.equals(other.orderDate))
			return false;
		if (orderedProducts == null) {
			if (other.orderedProducts != null)
				return false;
		} else if (!orderedProducts.equals(other.orderedProducts))
			return false;
		if (shippingAddress == null) {
			if (other.shippingAddress != null)
				return false;
		} else if (!shippingAddress.equals(other.shippingAddress))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}
	
	
	
}
