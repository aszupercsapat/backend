package hu.unideb.szupercsapat.projekt.model.user;

/**
 * Enum to determine user access rights. (ACL)
 * @author Levente Daradics
 *
 */
public enum UserAccessRights {
	CUSTOMER,
	APPLICATION_ADMIN,
	DEMI_GOD // has most rights
}
