package hu.unideb.szupercsapat.projekt.model.user;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import hu.unideb.szupercsapat.projekt.jpa.HashService;

/*
 * (Non-Javadoc)
 * Password needs to be hashed
 * TODO https://dzone.com/articles/storing-passwords-java-web
 * write a service that use hash algorythm to transfrom password
 * we only save hashed password
 */
@Entity
@Table(name="Users") // user is sql reserved keyword
public class User implements Serializable {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@NotNull
	@Column(nullable = false, unique= true) // validate on front-end
	private String emailAddress = "";
	
	@JsonIgnore
	@NotNull
	@Column(nullable = false)
	private String hashedPassword;
	
	@NotNull
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private UserAccessRights rights;
	
	@Embedded
	//@NotNull
	private UserData userData;
	
	@Transient
	private String transientPassword = "";
	
	public User()	{ }

	public User(String emailAddress, UserAccessRights rights, String transientPassword) throws NoSuchAlgorithmException {
		super();
		this.emailAddress = emailAddress;
		this.rights = rights;
		this.transientPassword = transientPassword;
		this.setHashedPassword(transientPassword, emailAddress);
	}
	
	public User(String emailAddress, UserAccessRights rights, String transientPassword, UserData userData) throws NoSuchAlgorithmException {
		this(emailAddress, rights, transientPassword);
		this.setUserData(userData);
	}

	public String getHashedPassword() {
		return hashedPassword;
	}

	public void setHashedPassword(String password, String email) throws NoSuchAlgorithmException {
		this.hashedPassword = HashService.getSecurePassword(password, email);
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) throws NoSuchAlgorithmException {
		this.emailAddress = emailAddress;
		this.setHashedPassword(this.transientPassword, this.emailAddress);
	}

	public UserAccessRights getRights() {
		return rights;
	}

	public void setRights(UserAccessRights rights) {
		this.rights = rights;
	}

	public Long getId() {
		return id;
	}

	public String getTransientPassword() {
		return transientPassword;
	}

	public void setTransientPassword(String transientPassword) throws NoSuchAlgorithmException {
		this.transientPassword = transientPassword;
		this.setHashedPassword(this.transientPassword, this.emailAddress);
	}

	public UserData getUserData() {
		return userData;
	}

	public void setUserData(UserData userData) {
		this.userData = userData;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((emailAddress == null) ? 0 : emailAddress.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof User))
			return false;
		User other = (User) obj;
		if (emailAddress == null) {
			if (other.emailAddress != null)
				return false;
		} else if (!emailAddress.equals(other.emailAddress))
			return false;
		return true;
	}

	
	
}
