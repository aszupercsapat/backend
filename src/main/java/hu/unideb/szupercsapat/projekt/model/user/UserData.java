package hu.unideb.szupercsapat.projekt.model.user;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import hu.unideb.szupercsapat.projekt.model.address.HunAddress;

/*
 * (Non-Javadoc) 
 * This will probably be embeddable
 */

@Embeddable
public class UserData {
	
	@NotNull
	@Column(nullable = false)
	private String lastName;
	
	@NotNull
	@Column(nullable = false)
	private String firstName; //keresztn�v
	
	@NotNull
	@Column(nullable = false)
	private String phoneNumber; 
	
	@NotNull
	@Column(nullable = false)
	private HunAddress address;

	public UserData() {
		super();
	}

	public UserData(String lastName, String firstName, String phoneNumber, HunAddress address) {
		super();
		this.lastName = lastName;
		this.firstName = firstName;
		this.phoneNumber = phoneNumber;
		this.address = address;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public HunAddress getAddress() {
		return address;
	}

	public void setAddress(HunAddress address) {
		this.address = address;
	}
	
	public void update(UserData otherUserData)	{
		if( !this.equals(otherUserData) )	{
			if(this.getPhoneNumber().equals(otherUserData.getPhoneNumber()))	{
				this.setPhoneNumber(otherUserData.getPhoneNumber());
			}
			if(this.getFirstName().equals(otherUserData.getFirstName()))	{
				this.setFirstName(otherUserData.getFirstName());
			}
			if(this.getLastName().equals(otherUserData.getLastName()))	{
				this.setLastName(otherUserData.getLastName());
			}
			if(this.getAddress().equals(otherUserData.getAddress()))	{
				//this.setAddress(otherUserData.getAddress());
				address.update(otherUserData.getAddress());
			}
		}
		else	{
			return;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof UserData))
			return false;
		UserData other = (UserData) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		return true;
	}
	
	
}
