package hu.unideb.szupercsapat.projekt.model.order;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import hu.unideb.szupercsapat.projekt.model.products.Product;

@Entity
public class OrderedProduct {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@NotNull
	@Column(nullable = false)
	private Long quantity;
	
	@ManyToOne( fetch = FetchType.EAGER)
	@JoinColumn( name = "PRODUCT_ID", nullable = false)
	private Product product;
	
	// https://stackoverflow.com/questions/13370221/jpa-hibernate-detached-entity-passed-to-persist
	@ManyToOne(  fetch = FetchType.EAGER, cascade = { /*CascadeType.PERSIST, */CascadeType.MERGE })
	private Order order;

	public OrderedProduct() {
		super();
	}

	public OrderedProduct(Long quantity, Product product, Order order) {
		super();
		this.quantity = quantity;
		this.product = product;
		this.order = order;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Long getId() {
		return id;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 * 
	 * hashCode and equals calculated by product and quantity  
	 */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof OrderedProduct))
			return false;
		OrderedProduct other = (OrderedProduct) obj;
		if (product == null) {
			if (other.product != null)
				return false;
		} else if (!product.equals(other.product))
			return false;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity))
			return false;
		return true;
	}
	
	
	
}
