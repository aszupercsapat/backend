package hu.unideb.szupercsapat.projekt.model.products;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/*
 * (Non-Javadoc)
 * embeddable
 */

@Embeddable
public class ProductDescription {
	
	@NotNull
	@Column(nullable = false)
	private String manufacturer;
	
	@NotNull
	@Column(nullable = false)
	private String name;
	
	@NotNull
	@Column(nullable = false)
	private Long priceInHUF;
	
	@NotNull
	@Column(nullable = false)
	private String shortDescription;

	public ProductDescription() {
		super();
	}

	public ProductDescription(String manufacturer, String name, Long priceInHUF, String shortDescription) {
		super();
		this.manufacturer = manufacturer;
		this.name = name;
		this.priceInHUF = priceInHUF;
		this.shortDescription = shortDescription;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getPriceInHUF() {
		return priceInHUF;
	}

	public void setPriceInHUF(Long priceInHUF) {
		this.priceInHUF = priceInHUF;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((manufacturer == null) ? 0 : manufacturer.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((priceInHUF == null) ? 0 : priceInHUF.hashCode());
		result = prime * result + ((shortDescription == null) ? 0 : shortDescription.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ProductDescription))
			return false;
		ProductDescription other = (ProductDescription) obj;
		if (manufacturer == null) {
			if (other.manufacturer != null)
				return false;
		} else if (!manufacturer.equals(other.manufacturer))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (priceInHUF == null) {
			if (other.priceInHUF != null)
				return false;
		} else if (!priceInHUF.equals(other.priceInHUF))
			return false;
		if (shortDescription == null) {
			if (other.shortDescription != null)
				return false;
		} else if (!shortDescription.equals(other.shortDescription))
			return false;
		return true;
	}
	
	
}
