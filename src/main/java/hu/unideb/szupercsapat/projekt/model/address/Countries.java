package hu.unideb.szupercsapat.projekt.model.address;

/**
 * List of Supported Countries.
 * @author Levente Daradics
 *
 */
public enum Countries {
	HUNGARY ("Hungary");
	
	private String stringRepresentation;
	
	public String getStringRepresentation()	{
		return this.stringRepresentation;
	}
	
	private Countries(String countryName) {
		this.stringRepresentation = countryName;
	}
}
