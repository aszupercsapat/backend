package hu.unideb.szupercsapat.projekt.model.products;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
public class Category implements Serializable, Cloneable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	@Column(nullable = false, unique = true)
	private String name;
	
	//probably bi-directional
	// for bidirectional we would need these
	//@OneToMany(mappedBy = "category", fetch = FetchType.LAZY)
	//private List <SubCategory> subCategory = new ArrayList<>();
	
	// bidirectional many to many would look like this:
	// TODO leave FetchType.LAZY and find another solution leaving FetchType to EAGER is a fail-safe solution
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, })
	@JoinTable(
			name = "CATEGORY_SUBCATEGORY",
			joinColumns = @JoinColumn(name = "CATEGORY_ID"),
			inverseJoinColumns = @JoinColumn(name = "SUBCATEGORY_ID")
	)
	/*
	@JsonIdentityInfo(
			  generator = ObjectIdGenerators.PropertyGenerator.class, 
			 // generator = ObjectIdGenerators.IntSequenceGenerator.class, 
			  //generator =   ObjectIdGenerators.UUIDGenerator.class,
			  property = "id")
			  */
	@JsonManagedReference 
	private Set<SubCategory> subCategories = new HashSet<SubCategory>();

	public Category() {
		super();
	}
	
	
	public Category(Category c)	{
		this.id = c.id;
		this.name = c.name;
		
		for(SubCategory sc : Collections.unmodifiableSet( c.getSubCategories() ))	{
			SubCategory scIn = new SubCategory(sc.getId());
			scIn.setName(sc.getName());
			this.subCategories.add(scIn);
		}
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<SubCategory> getSubCategories() {
		return subCategories;
	}

	public void setSubCategories(Set<SubCategory> subCategories) {
		this.subCategories = subCategories;
	}

	public Long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Category))
			return false;
		Category other = (Category) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
	    return super.clone();
	}
	
	
}
