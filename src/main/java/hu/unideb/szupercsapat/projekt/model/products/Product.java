package hu.unideb.szupercsapat.projekt.model.products;


import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/*
 * (Non-Javadoc)
 * Entity
 */

@Entity
public class Product {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne( fetch = FetchType.EAGER )
	@JoinColumn( name = "CATEGORY_ID", nullable = false)
	private Category category;
	
	@ManyToOne( fetch = FetchType.EAGER )
	@JoinColumn( name = "SUBCATEGORY_ID", nullable = false)
	private SubCategory subCategory;
	
	// embedded
	@Embedded
	private ProductDescription productDescription;

	public Product() {
		super();
	}

	public Product(Category category, SubCategory subCategory, ProductDescription productDescription) {
		super();
		this.category = category;
		this.subCategory = subCategory;
		this.productDescription = productDescription;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category categories) {
		this.category = categories;
	}

	public SubCategory getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(SubCategory subCategories) {
		this.subCategory = subCategories;
	}

	public ProductDescription getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(ProductDescription productDescription) {
		this.productDescription = productDescription;
	}

	public Long getId() {
		return id;
	}

	/**
	 * Calculated by product description.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((productDescription == null) ? 0 : productDescription.hashCode());
		return result;
	}

	/**
	 * Calculated by product description.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Product))
			return false;
		Product other = (Product) obj;
		if (productDescription == null) {
			if (other.productDescription != null)
				return false;
		} else if (!productDescription.equals(other.productDescription))
			return false;
		return true;
	}
	
	
	
}
