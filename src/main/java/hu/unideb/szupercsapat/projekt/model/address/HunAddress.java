package hu.unideb.szupercsapat.projekt.model.address;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import javax.validation.constraints.*;

/*
 * (Non-Javadoc)
 * Ideally there would be an Address interface with an international postal standard.
 * This class would implement the Address interface.
 * 
 * JPA notes: this will probably be @Embeddable
 */
/**
 * 
 * @author Levente Daradics
 *
 */
@Embeddable
public class HunAddress implements Address {
	
	/* 4 digit number */
	@DecimalMax(value = "9999")
	@DecimalMin(value = "0", inclusive = false)
	@NotNull
	@Column(nullable = false)
	private Integer postalCode;
	
	//@Transient
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private final Countries countryEnum = Countries.HUNGARY;
	
	@NotNull
	@Column(nullable = false)
	private String city;
	
	@NotNull
	@Column(nullable = false)
	private String nameOfPublicPlace;	// name of (square or street or avenue etc...)
	
	@Column
	private String typeOfPublicPlace;	//street, square, avenue... etc...
	
	@NotNull
	@Column(nullable = false)
	private String streetNumber;
	
	@Column
	private Integer floorNumber;
	
	@Column
	private String door;

	public HunAddress() {
		super();
	}

	public HunAddress(Integer postalCode, String city, String nameOfPublicPlace, String typeOfPublicPlace,
			String streetNumber, Integer floorNumber, String door) {
		super();
		this.postalCode = postalCode;
		this.city = city;
		this.nameOfPublicPlace = nameOfPublicPlace;
		this.typeOfPublicPlace = typeOfPublicPlace;
		this.streetNumber = streetNumber;
		this.floorNumber = floorNumber;
		this.door = door;
	}
	
	public static HunAddress update(HunAddress willBeUpdated, HunAddress withThisOne )	{
		
		if(!willBeUpdated.equals(withThisOne))	{
			HunAddress newOne = new HunAddress(); 
			
			newOne.setPostalCode(willBeUpdated.getPostalCode());
			if(!willBeUpdated.getPostalCode().equals(withThisOne.getPostalCode()))	{
				newOne.setPostalCode(withThisOne.getPostalCode());
			}
			
			newOne.setCity(willBeUpdated.getCity());
			if(!willBeUpdated.getCity().equals(withThisOne.getCity()))	{
				newOne.setCity(withThisOne.getCity());
			}
			
			newOne.setNameOfPublicPlace(willBeUpdated.getNameOfPublicPlace());
			if(!willBeUpdated.getNameOfPublicPlace().equals(withThisOne.getNameOfPublicPlace()))	{
				newOne.setNameOfPublicPlace(withThisOne.getNameOfPublicPlace());
			}
			
			newOne.setTypeOfPublicPlace(willBeUpdated.getTypeOfPublicPlace());
			if(!willBeUpdated.getTypeOfPublicPlace().equals(withThisOne.getTypeOfPublicPlace()))	{
				newOne.setTypeOfPublicPlace(withThisOne.getTypeOfPublicPlace());
			}
			
			newOne.setStreetNumber(willBeUpdated.getStreetNumber());
			if(!willBeUpdated.getStreetNumber().equals(withThisOne.getStreetNumber()))	{
				newOne.setStreetNumber(withThisOne.getStreetNumber());
			}
			
			newOne.setFloorNumber(willBeUpdated.getFloorNumber());
			if(!willBeUpdated.getFloorNumber().equals(withThisOne.getFloorNumber()))	{
				newOne.setFloorNumber(withThisOne.getFloorNumber());
			}
			
			newOne.setDoor(willBeUpdated.getDoor());
			if(!willBeUpdated.getDoor().equals(withThisOne.getDoor()))	{
				newOne.setDoor(withThisOne.getDoor());
			}
			
			return newOne;
		}
		else	{
			return willBeUpdated;
		}
		
	}
	
	public void update( HunAddress withThisOne )	{
		
		if(!this.equals(withThisOne))	{
			
			if(!this.getPostalCode().equals(withThisOne.getPostalCode()))	{
				this.setPostalCode(withThisOne.getPostalCode());
			}
			
			if(!this.getCity().equals(withThisOne.getCity()))	{
				this.setCity(withThisOne.getCity());
			}
			
			if(!this.getNameOfPublicPlace().equals(withThisOne.getNameOfPublicPlace()))	{
				this.setNameOfPublicPlace(withThisOne.getNameOfPublicPlace());
			}
			
			if(!this.getTypeOfPublicPlace().equals(withThisOne.getTypeOfPublicPlace()))	{
				this.setTypeOfPublicPlace(withThisOne.getTypeOfPublicPlace());
			}
			
			if(!this.getStreetNumber().equals(withThisOne.getStreetNumber()))	{
				this.setStreetNumber(withThisOne.getStreetNumber());
			}
			
			if(!this.getFloorNumber().equals(withThisOne.getFloorNumber()))	{
				this.setFloorNumber(withThisOne.getFloorNumber());
			}
			
			if(!this.getDoor().equals(withThisOne.getDoor()))	{
				this.setDoor(withThisOne.getDoor());
			}
			
		}
		
	}

	public Integer getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(Integer postalCode) {
		this.postalCode = postalCode;
	}

	public Countries getCountryEnum() {
		return countryEnum;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getNameOfPublicPlace() {
		return nameOfPublicPlace;
	}

	public void setNameOfPublicPlace(String nameOfPublicPlace) {
		this.nameOfPublicPlace = nameOfPublicPlace;
	}

	public String getTypeOfPublicPlace() {
		return typeOfPublicPlace;
	}

	public void setTypeOfPublicPlace(String typeOfPublicPlace) {
		this.typeOfPublicPlace = typeOfPublicPlace;
	}

	public String getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	public Integer getFloorNumber() {
		return floorNumber;
	}

	public void setFloorNumber(Integer floorNumber) {
		this.floorNumber = floorNumber;
	}

	public String getDoor() {
		return door;
	}

	public void setDoor(String door) {
		this.door = door;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((countryEnum == null) ? 0 : countryEnum.hashCode());
		result = prime * result + ((door == null) ? 0 : door.hashCode());
		result = prime * result + ((floorNumber == null) ? 0 : floorNumber.hashCode());
		result = prime * result + ((nameOfPublicPlace == null) ? 0 : nameOfPublicPlace.hashCode());
		result = prime * result + ((postalCode == null) ? 0 : postalCode.hashCode());
		result = prime * result + ((streetNumber == null) ? 0 : streetNumber.hashCode());
		result = prime * result + ((typeOfPublicPlace == null) ? 0 : typeOfPublicPlace.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HunAddress other = (HunAddress) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (countryEnum != other.countryEnum)
			return false;
		if (door == null) {
			if (other.door != null)
				return false;
		} else if (!door.equals(other.door))
			return false;
		if (floorNumber == null) {
			if (other.floorNumber != null)
				return false;
		} else if (!floorNumber.equals(other.floorNumber))
			return false;
		if (nameOfPublicPlace == null) {
			if (other.nameOfPublicPlace != null)
				return false;
		} else if (!nameOfPublicPlace.equals(other.nameOfPublicPlace))
			return false;
		if (postalCode == null) {
			if (other.postalCode != null)
				return false;
		} else if (!postalCode.equals(other.postalCode))
			return false;
		if (streetNumber == null) {
			if (other.streetNumber != null)
				return false;
		} else if (!streetNumber.equals(other.streetNumber))
			return false;
		if (typeOfPublicPlace == null) {
			if (other.typeOfPublicPlace != null)
				return false;
		} else if (!typeOfPublicPlace.equals(other.typeOfPublicPlace))
			return false;
		return true;
	}
	
	
}
