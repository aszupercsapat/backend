package hu.unideb.szupercsapat.projekt.app;


import hu.unideb.szupercsapat.projekt.rest.service.CategoriesResource;
import hu.unideb.szupercsapat.projekt.rest.service.OrdersResource;
import hu.unideb.szupercsapat.projekt.rest.service.ProductResource;
import hu.unideb.szupercsapat.projekt.rest.service.UserResource;
import org.jboss.resteasy.plugins.interceptors.CorsFilter;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;


@ApplicationPath("/webshop")
public class Webshop extends Application {

    private Set<Object> singletons = new HashSet<Object>();

    private Set<Class<?>> empty = new HashSet<Class<?>>();

    public Webshop() {
    	// add singleton classes
        singletons.add(new CategoriesResource());
        singletons.add(new ProductResource());
        singletons.add(new UserResource());
        singletons.add(new OrdersResource());
        
        //CORS filter
        singletons.add(returnCorsFilterWithLocalhostEnabled());
    }

    @Override
    public Set<Class<?>> getClasses() {
        return empty;
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
    
    private static CorsFilter returnCorsFilterWithLocalhostEnabled()	{
    	CorsFilter filter = new CorsFilter();
        filter.getAllowedOrigins().add("http://localhost:4200");
        filter.getAllowedOrigins().add("https://localhost:4200");
        filter.getAllowedOrigins().add("http://127.0.0.1:4200");
        filter.getAllowedOrigins().add("https://127.0.0.1:4200");
        
        return filter;
    }
}


