package hu.unideb.szupercsapat.projekt.jpa;

import java.security.NoSuchAlgorithmException;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import hu.unideb.szupercsapat.projekt.model.user.User;
import hu.unideb.szupercsapat.projekt.model.user.UserAccessRights;

public class UserCRUD {

	private static EntityManager em = EntityManagerUtil.getEntityManager();
	
	public static User retreive(Long id)	{
		
		User output = null;
		
		EntityManager em = EntityManagerUtil.getEntityManager();
		
		EntityTransaction tx = em.getTransaction();
		
		try	{
			
			tx.begin();
			
			output = em.find(User.class, id);
			
			em.flush();
			tx.commit();
			
		}
		catch (NoResultException ex) {
            tx.rollback();
		}
		catch(Exception ex)	{
			tx.rollback();
			throw new RuntimeException(ex);
		} 
		finally	{
			em.close();
		}
		
		return output ;
	}
	
	/**
	 * 
	 * @param transientUser
	 * @return an User with Persistent state, or null if something goes wrong
	 */
	public static User create(User transientUser)	{
		
		User persistentUser = null; 
		
		EntityManager em = EntityManagerUtil.getEntityManager();
		
		EntityTransaction tx = em.getTransaction();
		
		if( transientUser.getId() == null)	{

			try	{
				
				tx.begin();
				
				em.persist(transientUser);
				
				em.flush();
				tx.commit();
				
				persistentUser = transientUser;
			}
			catch(Exception ex)	{
				tx.rollback();
				throw new RuntimeException(ex);
			}
			finally	{
				em.close();
			}

		}
		else	{
			try	{
				
				tx.begin();
				
				persistentUser = em.find(User.class, transientUser.getId());
				
				em.flush();
				tx.commit();
			}
			catch (NoResultException ex) {
	            tx.rollback();
	            persistentUser = null;
			}
			catch(Exception ex)	{
				tx.rollback();
				throw new RuntimeException(ex);
			}
			finally	{
				em.close();
			}
			

		}
		
		return persistentUser ;
		
	}
	
	public static User create( String email, UserAccessRights rights, String password ) throws NoSuchAlgorithmException	{
		
		User transientUser = new User(email, rights, password);
		
		User persistentUser = null;
		
		EntityManager em = EntityManagerUtil.getEntityManager();
		
		EntityTransaction tx = em.getTransaction();
		
		try	{
			
			tx.begin();
			
			em.persist(transientUser);
			
			em.flush();
			tx.commit();
			
			persistentUser = transientUser;
			
			transientUser = null;
		}
		catch(Exception ex)	{
			tx.rollback();
			throw new RuntimeException(ex);
		}
		finally	{
			em.close();
		}
		
		return persistentUser;
	}
	
	/**
	 * 
	 * @param userWithNewerDatas
	 * @return the updated persistent User if the transaction is successful, null otherwise
	 */
	public static User update(User userWithNewerDatas)	{
		
		Long id = userWithNewerDatas.getId();
		
		EntityManager em = EntityManagerUtil.getEntityManager();
		
		EntityTransaction tx = em.getTransaction();
		
		User updatedPersistentUser = null;
		
		User oldPersistentUser = retreive(id);
		
		try	{
			
			tx.begin();
			
			if(!oldPersistentUser.getEmailAddress().equals(userWithNewerDatas.getEmailAddress()))	{
				oldPersistentUser.setEmailAddress(userWithNewerDatas.getEmailAddress());
				oldPersistentUser.setHashedPassword(oldPersistentUser.getTransientPassword(), oldPersistentUser.getEmailAddress());
			}
			if(!oldPersistentUser.getTransientPassword().equals(userWithNewerDatas.getTransientPassword()))	{
				oldPersistentUser.setTransientPassword(userWithNewerDatas.getTransientPassword());
				oldPersistentUser.setHashedPassword(oldPersistentUser.getTransientPassword(), oldPersistentUser.getEmailAddress());
			}
			if(!oldPersistentUser.getRights().equals(userWithNewerDatas.getRights()))	{
				oldPersistentUser.setRights(userWithNewerDatas.getRights());
			}
			if(!oldPersistentUser.getUserData().equals(userWithNewerDatas.getUserData()))	{
				oldPersistentUser.getUserData().update(userWithNewerDatas.getUserData());
			}
			
			updatedPersistentUser = em.merge(oldPersistentUser);
			
			em.flush();
			tx.commit();
			
		}
		catch(Exception ex)	{
			tx.rollback();
			throw new RuntimeException(ex);
		}
		finally	{
			em.close();
		}
		
		return updatedPersistentUser;
	}
	
	/**
	 * 
	 * @param deletable
	 * @return a User in deleted state if the transaction successfully completed, null otherwise
	 */
	public static User delete(User deletable)	{
		
		EntityManager em = EntityManagerUtil.getEntityManager();
		
		EntityTransaction tx = em.getTransaction();
		
		User removedStateUser = null;
		
		try	{
			
			tx.begin();
			
			em.remove(deletable);
			
			removedStateUser = deletable;
			
			em.flush();
			tx.commit();
			
		}
		catch(Exception ex)	{
			tx.rollback();
			throw new RuntimeException(ex);
		}
		finally	{
			em.close();
		}
		
		return removedStateUser;
	}
	
	/**
	 * Deletes the User if it exists in the database. Otherwise returns null.
	 * @param id
	 * @return
	 */
	public static User delete(Long id)	{
		
		EntityManager em = EntityManagerUtil.getEntityManager();
		
		EntityTransaction tx = em.getTransaction();
		
		User deletable = null;//retreive(id);
		
		try	{
			
			tx.begin();
			
			deletable = em.find(User.class, id);
			
			if(deletable != null)	{
				em.remove(deletable);
			}
			
			em.flush();
			tx.commit();
			
		}
		catch (NoResultException ex) {
            tx.rollback();
		}
		catch(Exception ex)	{
			tx.rollback();
			throw new RuntimeException(ex);
		}finally	{
			em.close();
		}
		
		
		return deletable;
	}
	
	public static User userLogin(User user)	throws UserLoginException {
		
		EntityManager em = EntityManagerUtil.getEntityManager();
		
		EntityTransaction tx = em.getTransaction();
		
		User userr = null;
		
		try	{
			
			tx.begin();
			
			TypedQuery<User> query = em.createQuery("select u from User u "
							+ "where u.emailAddress = :inputEmailAddr "
							+ "and "
							+ "u.hashedPassword = :inputHashedPW", User.class);
			
			
			query.setParameter("inputEmailAddr", user.getEmailAddress());
			String hashedPW= HashService.getSecurePassword(user.getTransientPassword(), user.getEmailAddress());
			query.setParameter("inputHashedPW", hashedPW);
			userr = query.getSingleResult();
			
			tx.commit();
			
			return userr;
		}
		catch (Exception ex)	{
			
			if(ex.getClass().equals(NoResultException.class ) )	{
				throw new UserLoginException("Wrong username or password.");
			}
			
			tx.rollback();
			
			throw new RuntimeException(ex);
		}
	}
}
