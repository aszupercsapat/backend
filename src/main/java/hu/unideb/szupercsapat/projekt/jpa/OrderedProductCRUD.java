package hu.unideb.szupercsapat.projekt.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import hu.unideb.szupercsapat.projekt.model.order.Order;
import hu.unideb.szupercsapat.projekt.model.order.OrderedProduct;
import hu.unideb.szupercsapat.projekt.model.products.Product;

public class OrderedProductCRUD {

	
	private static EntityManager em = EntityManagerUtil.getEntityManager();
	
	public static OrderedProduct create(OrderedProduct saveable)	{
		
		EntityManager em = EntityManagerUtil.getEntityManager();
		
		EntityTransaction tx = em.getTransaction();

        try {

            tx.begin();

            if(saveable.getId() == null)	{
            	
            	
            	if(!JPAUtils.isDetached(em, saveable))	{
            		em.persist(saveable);
            	}
            	
            }
            else	{	// if(saveable.getId() != null) 
            	if(JPAUtils.isDetached(em, saveable))	{
            		
            		saveable = em.merge(saveable);
            	}
            	
            	saveable = em.find(OrderedProduct.class, saveable.getId());
            }

            em.flush();
            tx.commit();

        } 
        catch(NoResultException ex)	{
        	tx.rollback();
        	saveable = null;
        }
        catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }
		
		return saveable;
	}
	
	public static OrderedProduct create(Long quantity, Product product, Order order)	{
		
		EntityManager em = EntityManagerUtil.getEntityManager();
		
		OrderedProduct op = new OrderedProduct(quantity, product, order);
		
		EntityTransaction tx = em.getTransaction();
		
		try {

            tx.begin();

            em.persist(op);

            em.flush();
            tx.commit();

        } catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }
		
		return op;
	}
	
	/**
	 * 
	 * @param id
	 * @return an OrderedProduct if found in database, null otherwise
	 */
	public static OrderedProduct retreive(Long id )	{
		
		EntityManager em = EntityManagerUtil.getEntityManager();
		
		OrderedProduct retreived = null;
		
		EntityTransaction tx = em.getTransaction();
		
		try {

            tx.begin();

            TypedQuery<OrderedProduct> query 
            		= em.createQuery("select op from OrderedProduct op"
            				+ " join fetch op.product"
            				+ " where op.id = :innputid"
            		
            		, OrderedProduct.class);
            query.setParameter("innputid", id);
            retreived = query.getSingleResult();
            
            
            //retreived = em.find(OrderedProduct.class, id);

            em.flush();
            tx.commit();

        }
		catch(NoResultException ex)	{
        	tx.rollback();
        	retreived = null;
        }
		catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }
		
		return retreived;
	}
	
	public static OrderedProduct update(OrderedProduct newerDatas)	{
		
		EntityManager em = EntityManagerUtil.getEntityManager();
		
		OrderedProduct oldDatas = retreive(newerDatas.getId());
		
		if(JPAUtils.isDetached(em, oldDatas) )	{
				
		
			em.detach(oldDatas);
		}
		
		if(JPAUtils.isDetached(em, newerDatas) )	{
			
			
			em.detach(newerDatas);
		}
		
		EntityTransaction tx = em.getTransaction();
		
		try {

            tx.begin();

            if(!oldDatas.getProduct().equals(newerDatas.getProduct()))	{
            	oldDatas.setProduct(newerDatas.getProduct());
            }
            if(!oldDatas.getQuantity().equals(newerDatas.getQuantity()))	{
            	oldDatas.setQuantity(newerDatas.getQuantity());
            }
            if(!oldDatas.getOrder().equals(newerDatas.getOrder()))	{
            	oldDatas.setOrder(newerDatas.getOrder());
            }
            oldDatas = em.merge(oldDatas);

            em.flush();
            tx.commit();

        } catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }
		
		return oldDatas;
	}
	
	public static OrderedProduct delete(OrderedProduct deletable)	{
		
		EntityManager em = EntityManagerUtil.getEntityManager();
		
		EntityTransaction tx = em.getTransaction();
		
		try {
			
			tx.begin();

			if(deletable.getId() != null)	{
				em.remove(deletable);
			}
            
			em.flush();
            tx.commit();

        } catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        }
		
		return deletable;
	}
	
	public static OrderedProduct delete(Long deletablesId)	{
		
		EntityManager em = EntityManagerUtil.getEntityManager();
		
		EntityTransaction tx = em.getTransaction();
		
		OrderedProduct deletable = retreive(deletablesId);
		
		try {
			
			tx.begin();

			if(deletable != null)	{
				if(deletable.getId() != null)	{
					em.remove(deletable);
				}
			}
			
			em.flush();
            tx.commit();

        } catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }
		
		return deletable;
	}
	
}
