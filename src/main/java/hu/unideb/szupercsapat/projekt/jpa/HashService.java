package hu.unideb.szupercsapat.projekt.jpa;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashService {
	
    public static String getSecurePassword(String password, String email) throws NoSuchAlgorithmException {
    	
        String generatedPassword;
        
        MessageDigest md = MessageDigest.getInstance("MD5");
        
        byte[] bytes = md.digest(password.concat(email).getBytes());
        
        StringBuilder sb = new StringBuilder();
        
        for (byte aByte : bytes) {
        	
            sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
        }
        generatedPassword = sb.toString();
        
        return generatedPassword;
    }
}
