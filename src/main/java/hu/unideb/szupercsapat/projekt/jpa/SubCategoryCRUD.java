package hu.unideb.szupercsapat.projekt.jpa;

import hu.unideb.szupercsapat.projekt.model.products.Category;
import hu.unideb.szupercsapat.projekt.model.products.SubCategory;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

public class SubCategoryCRUD implements Serializable{

    private static EntityManager em = EntityManagerUtil.getEntityManager();

    /*
     * Creates a category with the given name, or retrieves one if it already exists with the given name
     */
    public static SubCategory create(String name) {
    	
    	EntityManager em = EntityManagerUtil.getEntityManager();

        SubCategory output = null;

        //TransactionManager tx = EntityManagerUtil.getTransactionManager();
        
        EntityTransaction tx = em.getTransaction();
        
        output = retrieve(name);
        if( output != null)	{
        	return output;
        }
        else { //if (output == null) {
            try {
                tx.begin();
                //em.joinTransaction();

                if (output == null) {
                    output = new SubCategory();
                    output.setName(name);
                    em.persist(output);
                }
                tx.commit();
            } catch (Exception ex) {
                
                tx.rollback();
                throw new RuntimeException(ex);
            } finally	{
            	em.close();
            }
        }
        return output;

    }

    /*
     * Returns a Category with given name if it exist, otherwise returns null.
     */
    public static SubCategory retrieve(String name) {
    	
    	EntityManager em = EntityManagerUtil.getEntityManager();

        SubCategory subCategory = null;

        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();

            TypedQuery<SubCategory> query = em.createQuery("from SubCategory c where c.name = :name", SubCategory.class);
            query.setParameter("name", name);
            subCategory = query.getSingleResult();

            em.flush();
            tx.commit();
        } catch (NoResultException ex) {
            subCategory = null;
            tx.rollback();
        } catch (Throwable ex) {
            // Any other bad thing happens
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }

        return subCategory;
    }

    public static SubCategory retrieve(Long id) {

    	EntityManager em = EntityManagerUtil.getEntityManager();
    	
        EntityTransaction tx = em.getTransaction();

        SubCategory output = null;

        try {
            tx.begin();
            //em.joinTransaction();

            //output =  em.find(Category.class, id);

            TypedQuery<SubCategory> query = em.createQuery("from SubCategory c where c.id = :id", SubCategory.class);
            query.setParameter("id", id);
            output = query.getSingleResult();

            em.flush();
            tx.commit();
        }
        catch (NoResultException ex) {
        	output = null;
            tx.rollback();
        }
        catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }
        return output;
    }

    /*
     * Updates one if it already exists with the given ID.
     */
    public static SubCategory update(Long id, String newName) {
    	
    	EntityManager em = EntityManagerUtil.getEntityManager();

        EntityTransaction tx = em.getTransaction();

        SubCategory updateable = null;

        //updateable = retrieve(id);

        try {
            tx.begin();
            
            updateable = em.find(SubCategory.class, id);

            if (updateable == null) {

            } else {
                updateable.setName(newName);
            }
            em.flush();
            tx.commit();
        }
        catch (NoResultException ex) {
            tx.rollback();
        }
        catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }

        return updateable;
    }

    public static SubCategory delete(Long id) {

        SubCategory removed = null;
        
        EntityManager em = EntityManagerUtil.getEntityManager();

        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            
            removed = em.find(SubCategory.class, id);

            if (removed != null) {
                em.remove(removed);
            }

            em.flush();
            tx.commit();
        } 
        catch (NoResultException ex) {
            tx.rollback();
        }
        catch (Exception ex) {
            tx.rollback();
        } finally	{
        	em.close();
        }

        return removed;
    }

    public static SubCategory delete(SubCategory subCategory) {
        
        delete(subCategory.getId());

        return subCategory;
    }

    public static void deleteAll() {
        List<SubCategory> subCategories = SubCategoryCRUD.listSubCategories();
        for (SubCategory subcategory : subCategories) {
            SubCategoryCRUD.delete(subcategory.getId());
        }
    }

    public static List<SubCategory> listSubCategories() {
        //TODO TESTIT
        List<SubCategory> output = null;
        
        EntityManager em = EntityManagerUtil.getEntityManager();
        
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();

            TypedQuery<SubCategory> query = em.createQuery("select distinct sc from SubCategory sc join  Category c", SubCategory.class);
            output = query.getResultList();

            em.flush();
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }

        return output;
    }

    public static Set<Category> listCategoriesBySubCategory(Long subCategoryId) {

        //TODO TESTIT!!!!
        // enforcing persistent state
        SubCategory persistentSubCategory = retrieve(subCategoryId);

        return persistentSubCategory.getCategories();

    }

    public static Set<Category> listCategoriesBySubCategory(SubCategory subCategory) {

        //TODO TESTIT!!!!
        return listCategoriesBySubCategory(subCategory.getId());

    }

    public static Set<Category> addCategoryToSubCategory(SubCategory subCategoryToAddTo, Category categoryToAdd) {

        //TODO TESTIT!!!!
    	EntityManager em = EntityManagerUtil.getEntityManager();
    	
        Set<Category> persistentCategories = listCategoriesBySubCategory(subCategoryToAddTo);

        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            persistentCategories.add(categoryToAdd);
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
        } finally	{
        	em.close();
        }

        return persistentCategories;
    }

    public static EntityManager getEntityManager() {
        return em;
    }

}
