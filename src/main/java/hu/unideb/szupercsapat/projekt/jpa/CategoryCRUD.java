package hu.unideb.szupercsapat.projekt.jpa;

import hu.unideb.szupercsapat.projekt.model.products.Category;
import hu.unideb.szupercsapat.projekt.model.products.SubCategory;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CategoryCRUD {

    private static EntityManager em = EntityManagerUtil.getEntityManager();

    
    public static EntityManager getEm() {
		return em;
	}

	/*
     * Creates a category with the given name, or retrieves one if it already exists with the given name
     */
    public static Category create(String name) {

    	EntityManager em = EntityManagerUtil.getEntityManager();
    	
        Category output = null;

        //TransactionManager tx = EntityManagerUtil.getTransactionManager();
        EntityTransaction tx = em.getTransaction();

        output = retrieve(name);

        if (output == null) {
            try {
                tx.begin();
                //em.joinTransaction();

                if (output == null) {
                    output = new Category();
                    output.setName(name);
                    em.persist(output);
                }
                tx.commit();
            } catch (Exception ex) {
                ex.printStackTrace();
                tx.rollback();
                throw new RuntimeException(ex);
            } finally	{
            	em.close();
            }
        }
        return output;
    }

    /*
     * Returns a Category with given name if it exist, otherwise returns null.
     */
    public static Category retrieve(String name) {

    	EntityManager em = EntityManagerUtil.getEntityManager();
    	
        Category category = null;

        //TransactionManager tx = EntityManagerUtil.getTransactionManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            //em.joinTransaction();

            TypedQuery<Category> query = em.createQuery("from Category c where c.name = :name", Category.class);
            query.setParameter("name", name);
            category = query.getSingleResult();

            //if (tx.isActive() )
            tx.commit();
        } catch (NoResultException ex) {
            category = null;
            tx.rollback();
        } catch (Throwable ex) {
            // Any other bad thing happens
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }

        return category;
    }

    public static Category retrieve(Long id) {

    	EntityManager em = EntityManagerUtil.getEntityManager();
    	
        //TransactionManager tx = EntityManagerUtil.getTransactionManager();
        EntityTransaction tx = em.getTransaction();

        Category output = null;

        try {
            tx.begin();
			/*
			//em.joinTransaction();
			
			//output =  em.find(Category.class, id);
			
			TypedQuery<Category> query = em.createQuery("from Category c where c.id = :id", Category.class);
	        query.setParameter("id", id);
	        output = query.getSingleResult() ;
	        */

            output = em.find(Category.class, id);

            tx.commit();
        } 
        catch (NoResultException ex) {
            tx.rollback();
        }
        catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }
        return output;
    }

    /*
     * Updates one if it already exists with the given ID.
     */
    public static Category update(Long id, String newName) {

        //TransactionManager tx = EntityManagerUtil.getTransactionManager();
    	

        Category updateable = null;

        updateable = retrieve(id);
        
        EntityManager em = EntityManagerUtil.getEntityManager();
    	
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            //em.joinTransaction();

            if (updateable == null) {

            } else {
                updateable.setName(newName);
                em.merge(updateable);
            }
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }

        return updateable;
    }

    public static Category delete(Long id) {

    	EntityManager em = EntityManagerUtil.getEntityManager();
    	
        Category removed = null;

        //TransactionManager tx = EntityManagerUtil.getTransactionManager();
        

       // removed = retrieve(id);

        EntityTransaction tx = em.getTransaction();
        
        try {
            tx.begin();
            //em.joinTransaction();

            removed = em.find(Category.class, id);
            
            if (removed != null) {
                em.remove(removed);
            }

            em.flush();
            tx.commit();
        } 
        catch (NoResultException ex) {
            tx.rollback();
        }
        catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        }
        finally	{
        	em.close();
        }

        return removed;
    }

    public static Category delete(Category category) {
    	
    	EntityManager em = EntityManagerUtil.getEntityManager();

        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            //em.joinTransaction();

            if (category != null) {
                em.remove(category);
            }

            em.flush();
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        }
        finally	{
        	em.close();
        }

        return category;
    }

    public static void deleteAll() {
        List<Category> categories = CategoryCRUD.listCategories();
        for (Category category : categories) {
            delete(category.getId());
        }
    }
    
    public static List<Category> listCategories() {
    	
    	EntityManager em = EntityManagerUtil.getEntityManager();
    	
        List<Category> output = null;

        EntityTransaction tx = em.getTransaction();

        try {
        	
        	if(!tx.isActive())	{
        		tx.begin();
        		
        		TypedQuery<Category> query = em.createQuery("select distinct c from Category c join fetch c.subCategories sc", Category.class);
        		//TypedQuery<Category> query = em.createQuery("from Category c", Category.class);
                output = query.getResultList();

                em.flush();
                tx.commit();
                
        	}
            
        } catch (Exception ex) {
        	
        	if(tx.isActive())	{
        		tx.rollback();
        	}
            
            throw new RuntimeException(ex);
        }
        
        finally	{
        	em.close();
        }
        
        //clone the categories
        

        return Collections.unmodifiableList(output);
    }
    
    public static List<Category> listCategoriesDetached()	{
    	List<Category> listOfCategoriesPersistent = listCategories();
    	
    	List<Category> listOfDetachedCategories = new ArrayList<>();
    	
    	
    	for(Category cat : listOfCategoriesPersistent )	{
    		em.detach(cat);
    		//Cloner cloner = new Cloner();
    		//listOfDetachedCategories.add( cloner.deepClone(cat));
    		//listOfDetachedCategories.add( cloner.deepClone(cat));
    		
    		listOfDetachedCategories.add(new Category(cat));
    	}
    	
    	listOfDetachedCategories = Collections.unmodifiableList(listOfDetachedCategories);
    	
    	return listOfDetachedCategories;
    }

    public static Set<SubCategory> listSubCategoriesByCategory(Long categoryId) {
        
        EntityManager em = EntityManagerUtil.getEntityManager();
        
        EntityTransaction tx = em.getTransaction();
        
        List<SubCategory> output = null;
        
        try {


            if(!tx.isActive())	{
        		tx.begin();
        		
        		TypedQuery<SubCategory> query = em.createQuery("select distinct sc from SubCategory sc join fetch sc.categories c where c.id = :argid", SubCategory.class);
        		//TypedQuery<Category> query = em.createQuery("from Category c", Category.class);
        		query.setParameter("argid", categoryId);
                output = query.getResultList();
                
                em.flush();
                tx.commit();
        	}
            
        } catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        	
        }
        
        if(output != null)	{
        	return (Set<SubCategory>) new HashSet<SubCategory>( output );
        }
        else	{
        	return null;
        }

        
    }

    public static Set<SubCategory> listSubCategoriesByCategory(Category category) {

    	/*
    	EntityManager em = EntityManagerUtil.getEntityManager();
        
        EntityTransaction tx = em.getTransaction();

        //em.is

        try {

            tx.begin();

            category = em.find(Category.class, category.getId());
            
            em.flush();
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }
		*/

        return listSubCategoriesByCategory(category.getId());
        //return category.getSubCategories();

    }

    public static Set<SubCategory> addSubCategoryToCategory(Category categoryToAddTo, SubCategory subCategoryToAdd) {

        //TODO TESTIT!!!!

    	EntityManager em = EntityManagerUtil.getEntityManager();
    	
        Set<SubCategory> persistentSubCategories = null;

        EntityTransaction tx = em.getTransaction();

        try {

            tx.begin();
			
            categoryToAddTo = em.find(Category.class, categoryToAddTo.getId());

            persistentSubCategories = categoryToAddTo.getSubCategories();

            persistentSubCategories.add(subCategoryToAdd);

            em.merge(categoryToAddTo);

            em.flush();
            tx.commit();
            
        } catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }

        return persistentSubCategories;

    }

    public static EntityManager getEntityManager() {
        return EntityManagerUtil.getEntityManager();
    }

}
