package hu.unideb.szupercsapat.projekt.jpa.converters;

import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply=true)
public class LocalDateConverter implements AttributeConverter<LocalDate, Date> {
    @Override
    public Date convertToDatabaseColumn(LocalDate d){
        return d !=null ? Date.valueOf(d) : null;
    }

    @Override
    public LocalDate convertToEntityAttribute(Date d){
        return d !=null ? d.toLocalDate() : null;
    }

}