package hu.unideb.szupercsapat.projekt.jpa;

import javax.persistence.EntityManager;

import hu.unideb.szupercsapat.projekt.model.order.Order;
import hu.unideb.szupercsapat.projekt.model.order.OrderedProduct;
import hu.unideb.szupercsapat.projekt.model.products.Category;
import hu.unideb.szupercsapat.projekt.model.products.Product;
import hu.unideb.szupercsapat.projekt.model.products.SubCategory;
import hu.unideb.szupercsapat.projekt.model.user.User;

public class JPAUtils {

	public static boolean isDetached(EntityManager em, Object entity) {
		
		if( entity instanceof  Order)	{
			Order order = (Order) entity;
			return order.getId() != null  // must not be transient
			        && !em.contains(entity)  // must not be managed now
			        && em.find(Order.class, order.getId()) != null;  // must not have been removed
		}
		
		else if( entity instanceof Category)	{
			Category category = (Category) entity;
			return category.getId() != null  // must not be transient
			        && !em.contains(entity)  // must not be managed now
			        && em.find(Order.class, category.getId()) != null;  // must not have been removed
		}
		
		else if( entity instanceof SubCategory)	{
			SubCategory subCategory = (SubCategory) entity;
			return subCategory.getId() != null  // must not be transient
			        && !em.contains(entity)  // must not be managed now
			        && em.find(Order.class, subCategory.getId()) != null;  // must not have been removed
		}
		
		else if( entity instanceof Product)	{
			Product product = (Product) entity;
			return product.getId() != null  // must not be transient
			        && !em.contains(entity)  // must not be managed now
			        && em.find(Order.class, product.getId()) != null;  // must not have been removed
		}
		
		else if( entity instanceof User)	{
			User user = (User) entity;
			return user.getId() != null  // must not be transient
			        && !em.contains(entity)  // must not be managed now
			        && em.find(Order.class, user.getId()) != null;  // must not have been removed
		}
		
		else if( entity instanceof OrderedProduct)	{
			OrderedProduct orderedProduct = (OrderedProduct) entity;
			return orderedProduct.getId() != null  // must not be transient
			        && !em.contains(entity)  // must not be managed now
			        && em.find(Order.class, orderedProduct.getId()) != null;  // must not have been removed
		}
		
		else	{
			throw new IllegalArgumentException("Class Type: " + entity.getClass() + " is not supported in this Persistence Context.");
		}
	}
}
