package hu.unideb.szupercsapat.projekt.jpa;

import hu.unideb.szupercsapat.projekt.model.products.Category;
import hu.unideb.szupercsapat.projekt.model.products.Product;
import hu.unideb.szupercsapat.projekt.model.products.ProductDescription;
import hu.unideb.szupercsapat.projekt.model.products.SubCategory;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.List;

public class ProductCRUD {

    private static EntityManager em = EntityManagerUtil.getEntityManager();

    /**
     * @return a Product in persistent state or null
     */
    public static Product retreive(Long productId) {

        Product output = null;
        
        EntityManager em = EntityManagerUtil.getEntityManager();

        EntityTransaction tx = em.getTransaction();

        try {

            tx.begin();
			
			/*
			TypedQuery<Product> query = em.createQuery("select p from Product p join fetch Category c join fetch SubCategory sc where p.id = :theid", Product.class);
	        query.setParameter("theid", productId);
	        output = query.getSingleResult() ;
	        */

            output = em.find(Product.class, productId);

            em.flush();
            tx.commit();

        } 
        catch (NoResultException ex) {
            tx.rollback();
        }
        catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }

        return output;
    }
    
    /**
     * @return a Product in persistent state or null
     */
    public static Product retreiveNoTX(Long productId, EntityManager em) {

        Product output = null;

        try {
			
            output = em.find(Product.class, productId);

        } 
        catch (NoResultException ex) {
        	output = null;
        }

        return output;
    }

    public static List<Product> retreiveByCategoryIdAndSubCategoryId(Long categoryId, Long subCategoryId, int startPos, int maxNumOfMaxRetreive) {

    	EntityManager em = EntityManagerUtil.getEntityManager();
    	
        EntityTransaction tx = em.getTransaction();

        List<Product> products = null;

        try {

            tx.begin();

            TypedQuery<Product> query =
					/*
					em.createQuery("from Product p "		// error unexpected token
								+ "where p.category.id = :categoryId "
								+ "and p.subCategory.id = :subCategoryId", Product.class);
								*/ 
					
					/*
					em.createQuery("select p from Product p "
								+ "inner join p.category cat inner join p.subCategory subCat "
								+ "where cat.id = :categoryId "
								+ "and subCat.id = :subCategoryId", Product.class);
								*/

                    em.createQuery("select prod from Product prod "
                            + "join prod.category cat join prod.subCategory subCat "
                            + "where cat.id = :categoryId "
                            + "and subCat.id = :subCategoryId", Product.class);

            query.setParameter("categoryId", categoryId);
            query.setParameter("subCategoryId", subCategoryId);
            query.setFirstResult(startPos);
            query.setMaxResults(maxNumOfMaxRetreive);
            products = query.getResultList();

            em.flush();
            tx.commit();
        } catch (NoResultException ex) {
            tx.rollback();
            return null;
        } catch (Throwable ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }

        return products;
    }

    public static List<Product> retreiveByCategoryId(Long categoryId, int startPos, int maxNumOfMaxRetreive) {

    	EntityManager em = EntityManagerUtil.getEntityManager();
    	
        EntityTransaction tx = em.getTransaction();
        
        

        List<Product> products = null;

        try {

            tx.begin();

            TypedQuery<Product> query =
                    em.createQuery("from Product p "
                            + "where p.category.id = :categoryId", Product.class);
            query.setParameter("categoryId", categoryId);
            query.setFirstResult(startPos);
            query.setMaxResults(maxNumOfMaxRetreive);
            products = query.getResultList();

            em.flush();
            tx.commit();
        } catch (NoResultException ex) {
            tx.rollback();
        } catch (Throwable ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }

        return products;
    }

    /**
     * @return List of Products
     */
    public static List<Product> readAll() {
        //TODO: TEST
        List<Product> output = null;
        
        EntityManager em = EntityManagerUtil.getEntityManager();

        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            TypedQuery<Product> query = em.createQuery("from Product c ", Product.class);
            output = query.getResultList();
            
            em.flush();
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }

        return output;
    }
    
    /**
     * @return List of Products
     */
    public static List<Product> retreiveProductsOfAnyKind(int startPos, int maxNumOfMaxRetreive) {
        //TODO: TEST
        List<Product> output = null;
        
        EntityManager em = EntityManagerUtil.getEntityManager();

        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            TypedQuery<Product> query = em.createQuery(
            		"select p "
            		+ "from Product p "
            		+ "join fetch p.category "
            		+ "join fetch p.subCategory"
            		, Product.class);
            query.setFirstResult(startPos);
            query.setMaxResults(maxNumOfMaxRetreive);
            output = query.getResultList();
            
            em.flush();
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }

        return output;
    }

    /**
     * @param transientProduct
     * @return
     */
    public static Product create(Product transientProduct) {

        Product managedProduct = null;
        
        EntityManager em = EntityManagerUtil.getEntityManager();

        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();

            if (transientProduct.getId() == null) {
                em.persist(transientProduct);
            }
            
            em.flush();
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }


        managedProduct = transientProduct;

        return managedProduct;
    }

    /**
     * @param manufacturer
     * @param name
     * @param priceInHUF
     * @param shortDescription
     * @param category
     * @param subCategory
     * @return
     */
    public static Product create(String manufacturer, String name, Long priceInHUF, String shortDescription, Category category, SubCategory subCategory) {

        ProductDescription productDesc = new ProductDescription(manufacturer, name, priceInHUF, shortDescription);

        Product newTransientProduct = new Product(category, subCategory, productDesc);

        EntityManager em = EntityManagerUtil.getEntityManager();

        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            
            em.persist(newTransientProduct);
            
            em.flush();
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }

        return newTransientProduct;

    }

    public static Product updateProductDescription(Product input, ProductDescription productDescription) {

    	EntityManager em = EntityManagerUtil.getEntityManager();
    	
        EntityTransaction tx = em.getTransaction();

        try {

            tx.begin();

            if (em.contains(input)) {

                em.refresh(input);

                input.setProductDescription(productDescription);

                //em.merge(input)

            }else {
            	input = em.find(Product.class, input.getId());
            }

            em.flush();
            tx.commit();
        } 
        catch(NoResultException ex)	{
        	tx.rollback();
        }
        catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }

        return input;
    }

    public static Product updateProductCategory(Product product, Category category) {

    	EntityManager em = EntityManagerUtil.getEntityManager();
    	
        EntityTransaction tx = em.getTransaction();

        try {

            tx.begin();

            if (em.contains(product)) {

                em.refresh(product);

                product.setCategory(category);

                product = em.merge(product);

            }	else {
            	
            	product = em.find(Product.class, product.getId());
            	
            }

            em.flush();
            tx.commit();
        } 
        catch(NoResultException ex)	{
        	tx.rollback();
        }
        catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }

        return product;
    }

    public static Product updateProductSubCategory(Product product, SubCategory subCategory) {

    	EntityManager em = EntityManagerUtil.getEntityManager();
    	
        EntityTransaction tx = em.getTransaction();

        try {

            tx.begin();

            if (em.contains(product)) {

                em.refresh(product);

                product.setSubCategory(subCategory);

                product = em.merge(product);

            }
            else {
            	
            	product = em.find(Product.class, product.getId());
            	
            }
            
            em.flush();
            tx.commit();
        } 
        catch(NoResultException ex)	{
        	tx.rollback();
        }
        catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }

        return product;
    }

    /**
     * @param id
     * @return a Product in removed state
     */
    public static Product delete(Long id) {

    	EntityManager em = EntityManagerUtil.getEntityManager();
    	
        Product product = retreive(id);

        EntityTransaction tx = em.getTransaction();

        if(product != null)	{
        	try {
                tx.begin();

                
                em.remove(product);
                
                

                em.flush();
                tx.commit();
            } catch (Exception ex) {
                tx.rollback();
                throw new RuntimeException(ex);
            } finally	{
            	em.close();
            }

        }
        
        
        return product;

    }

    /**
     * @param product
     * @return a Product in removed state
     */
    public static Product deleteProduct(Product product) {

    	EntityManager em = EntityManagerUtil.getEntityManager();
    	
        EntityTransaction tx = em.getTransaction();

        try {

            tx.begin();

            /*
            if (em.contains(product)) {
				em.remove(product);
                
            }*/
            
            if(product != null)	{
            	if(product.getId() != null)	{
            		em.remove(product);
            	}
            }

            em.flush();
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }

        return product;

    }

    public static void deleteAll() {
        List<Product> products = readAll();
        for (Product product : products) {
            delete(product.getId());
        }
    }

    public static EntityManager getEntityManager() {
        return EntityManagerUtil.getEntityManager();
    }


}
