package hu.unideb.szupercsapat.projekt.jpa;

public class UserLoginException extends Exception {

	public UserLoginException(Exception ex) {
		super(ex);
	}

	public UserLoginException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1342356235234L;

	
}
