package hu.unideb.szupercsapat.projekt.jpa;

import hu.unideb.szupercsapat.projekt.model.address.HunAddress;
import hu.unideb.szupercsapat.projekt.model.order.Order;
import hu.unideb.szupercsapat.projekt.model.user.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

public class OrderCRUD {

	
	private static EntityManager em = EntityManagerUtil.getEntityManager();
	
	public static Order create(Order transientOrder)	{
		
		EntityManager em = EntityManagerUtil.getEntityManager();
		
		EntityTransaction tx = em.getTransaction();

        try {

            tx.begin();

            if(transientOrder.getId() != null)	{
            	if( em.find(Order.class, transientOrder.getId()) != null)	{
            		transientOrder = em.find(Order.class, transientOrder.getId());
            	}
            	else if(JPAUtils.isDetached(em, transientOrder))	{
            		transientOrder = em.merge(transientOrder);
            	}
            }
            else	{
            	em.persist(transientOrder);
            }

            em.flush();
            tx.commit();

        }
        catch (NoResultException ex) {
            tx.rollback();
        }
        catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }
		
		return transientOrder; // now persistent
	}
	
	
	public static Order create(User user, HunAddress billingAddress, HunAddress shippingAddress)	{
		
		EntityManager em = EntityManagerUtil.getEntityManager();
		
		Order transientOrder = new Order(user, billingAddress, shippingAddress);
		
		EntityTransaction tx = em.getTransaction();

        try {

            tx.begin();

            em.persist(transientOrder);

            em.flush();
            tx.commit();

        } catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }
		
		return transientOrder; // nor persistent
	}
	
	public static Order retreive(Long id )	{
		
		Order retreived = null;
		
		EntityManager em = EntityManagerUtil.getEntityManager();
		
		EntityTransaction tx = em.getTransaction();
		
		try {

            tx.begin();

            //https://stackoverflow.com/questions/30088649/multiple-join-fetch-in-one-jpql-query
            /*
            TypedQuery<Order> query = em.createQuery("select ord from Order ord "
            		+ " join fetch ord.user usr"
            		+ " join fetch ord.orderedProducts ops"
            		+ " join fetch ops.product oproduct"
            		+ " where ord.id = :ordid", Order.class);
            query.setParameter("ordid", id);
            retreived = query.getSingleResult();
            */
            
            retreived = em.find(Order.class, id);

            tx.commit();

        }
		catch(NoResultException ex)	{
			tx.rollback();
			retreived = null;
		}
		catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }
		
		return retreived;
	}
	
	public static Order update(Order newValues)	{
		
		Order oldDatas = retreive(newValues.getId());
		
		EntityManager em = EntityManagerUtil.getEntityManager();
		
		em.detach(oldDatas);
		
		em.detach(newValues);
		
		
		EntityTransaction tx = em.getTransaction();
		
		try {

            tx.begin();

            if(!oldDatas.getShippingAddress() .equals(newValues.getShippingAddress()))	{
            	oldDatas.setShippingAddress(newValues.getShippingAddress());
            }
            if(!oldDatas.getBillingAddress() .equals(newValues.getBillingAddress()))	{
            	oldDatas.setBillingAddress(newValues.getBillingAddress());
            }
            if(!oldDatas.getUser() .equals(newValues.getUser()))	{
            	oldDatas.setUser(newValues.getUser());
            }
            
            //TODO ordered products
            
            //Order date is not updateable
            //OrderedProducts is not managed in Order

            oldDatas = em.merge(oldDatas);

            em.flush();
            tx.commit();

        } catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        } finally	{
        	em.close();
        }
		
		return oldDatas;
	}
	
	public static Order delete(Long deletablesId)	{
		
		EntityManager em = EntityManagerUtil.getEntityManager();
		
		EntityTransaction tx = em.getTransaction();
		
		Order deletable = retreive(deletablesId);
		
		try {
			
			tx.begin();

			if(deletable.getId() != null)	{
				em.remove(deletable);
			}

			em.flush();
            tx.commit();

        } catch (Exception ex) {
            tx.rollback();
            throw new RuntimeException(ex);
        }
		finally	{
        	em.close();
        }
		
		return deletable;
	}
	
	/**
	 * For incoming OrderDTO-s. It will also update.
	 * @param dto
	 * @return
	 */
	/*
	public static Order dtoToOrder(OrderDTO dto)	{
		
		EntityManager em = EntityManagerUtil.getEntityManager();
		
		EntityTransaction tx = em.getTransaction();
		
		Order order = retreive(dto.getId());
		
		if( order != null)	{
			
			try	{
				tx.begin();
				
				em.refresh(order);
				
				if( !order.getBillingAddress().equals(dto.getBillingAddress()) )	{
					order.setBillingAddress(dto.getBillingAddress());
				}
				if( !order.getShippingAddress().equals(dto.getShippingAddress()) )	{
					order.setShippingAddress(dto.getShippingAddress());
				}
				if( !order.getUser().getId().equals(dto.getUserId()))	{
					order.setUser(em.find(User.class, dto.getUserId()));
				}
				
				Set<Long> ordersOrderedProductsIdsAsSet = 
							order.getOrderedProducts()
						.stream()
						.map( (op) -> op.getId() )
						.collect(Collectors.toSet());
				
				Set<Long> dtosOrderedProductIdsAsSet = new HashSet<Long>(dto.getOrderedProductIds());
				
				if( !ordersOrderedProductsIdsAsSet.equals(dtosOrderedProductIdsAsSet) )	{
					
					for( Long orderedProductId : dtosOrderedProductIdsAsSet)	{
						OrderedProduct op = em.find(OrderedProduct.class, orderedProductId);
						op.setOrder(order);
						em.merge(op);
					}
					
					List<Long> ordersOrderedProductsIdsAsList = new ArrayList<Long>( ordersOrderedProductsIdsAsSet );
					List<Long> dtosOrderedProductIdsAsList = new ArrayList<Long>( dtosOrderedProductIdsAsSet );
					
					List<Long> subtracted = ListUtils.subtract(ordersOrderedProductsIdsAsList, dtosOrderedProductIdsAsList);
					
					for( Long theId : subtracted)	{
						OrderedProduct op = em.find(OrderedProduct.class, theId);
						op.setOrder(null);
						em.merge(op);
					}
				}
				
				tx.commit();
			}
			catch(Exception ex) {
				
				tx.rollback();
				throw new RuntimeException();
			}
			finally	{
				em.close();
			}
			
		}
		
		// updating the order
		else	{	// if (order == null)
			
			Order newOrder = new Order();
			
			newOrder.setBillingAddress(dto.getBillingAddress()); 
			
			newOrder.setShippingAddress(dto.getShippingAddress());
			
			if( dto.getOrderDate() == null)	{
				
				newOrder.setOrderDate(LocalDate.now());
				
			}
			
			else	{
				
				newOrder.setOrderDate(dto.getOrderDate());
				
			}
			
			List<Long> orderedProductIds = dto.getOrderedProductIds();
			
			for( Long orderedProductId : orderedProductIds )	{
				
				OrderedProduct actualOrderedProduct = OrderedProductCRUD.retreive(orderedProductId);
				
				if( actualOrderedProduct != null)	{
					
					actualOrderedProduct.setOrder(newOrder);
					
				}	
			}
			
			try	{
				tx.begin();
				
				em.persist(newOrder);
				
				order = newOrder;
				
				em.flush();
				tx.commit();
			}
			catch(Exception ex)	{
				tx.rollback();
				throw new RuntimeException(ex);
			}
			finally	{
				em.close();
			}
			
		}
		
		return order;
	} */
}
