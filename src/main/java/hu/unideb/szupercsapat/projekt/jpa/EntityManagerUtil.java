package hu.unideb.szupercsapat.projekt.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.transaction.TransactionManager;

public class EntityManagerUtil {
	
	//@PersistenceContext
	@PersistenceUnit(unitName="WebshopPU")
	private static final EntityManagerFactory entityManagerFactory;
	
	static {
		try {
			entityManagerFactory = Persistence.createEntityManagerFactory("WebshopPU");

		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
	
	public static EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();

	}
	
	/*
	public static TransactionManager getTransactionManager()	{
		return com.arjuna.ats.jta.TransactionManager.transactionManager();
	}
	*/
}