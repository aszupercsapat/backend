package hu.unideb.szupercsapat.projekt.rest.dto;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import hu.unideb.szupercsapat.projekt.jpa.EntityManagerUtil;
import hu.unideb.szupercsapat.projekt.jpa.OrderCRUD;
import hu.unideb.szupercsapat.projekt.jpa.OrderedProductCRUD;
import hu.unideb.szupercsapat.projekt.jpa.ProductCRUD;
import hu.unideb.szupercsapat.projekt.model.order.Order;
import hu.unideb.szupercsapat.projekt.model.order.OrderedProduct;
import hu.unideb.szupercsapat.projekt.model.products.Product;

public class OrderedProductDTO {

	private final Long id;
	
	private final Long quantity;
	
	private final Long productId;
	
	private final Long orderId;
	
	@JsonCreator
	public OrderedProductDTO(@JsonProperty("id")Long id,
			@JsonProperty("quantity") Long quantity, 
			@JsonProperty("productId") Long productId, 
			@JsonProperty("orderId")Long orderId) {
		super();
		this.id = id;
		this.quantity = quantity;
		this.productId = productId;
		this.orderId = orderId;
	}
	
	public Long getId() {
		return id;
	}

	public Long getQuantity() {
		return quantity;
	}

	public Long getProductId() {
		return productId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public static OrderedProductDTO toOrderedProductDTO( OrderedProduct op )	{
		
		return new OrderedProductDTO(op.getId(), op.getQuantity(), op.getProduct().getId(), op.getOrder().getId());
	}
	
	public static OrderedProduct fromDTOToOrderedProduct( OrderedProductDTO dto )	{
		
		OrderedProduct output = null;
		
		if( dto.getId() != null )	{
			OrderedProduct op = OrderedProductCRUD.retreive(dto.getId());
			
			// update
			if( op != null ) {
				
				EntityManager em = EntityManagerUtil.getEntityManager();
				
				EntityTransaction tx = em.getTransaction();
				
				try	{
					
					tx.begin();
					
					if( !dto.getQuantity().equals(op.getQuantity())  )	{
						op.setQuantity(dto.getQuantity());
					}
					
					if( !dto.getProductId().equals(op.getProduct().getId()) )	{
						
						Product retreivedProduct = em.find(Product.class, dto.getProductId());
						
						op.setProduct(retreivedProduct);
					}
					
					if( dto.getOrderId() != null)	{
						
						if( !dto.getOrderId().equals(op.getOrder().getId() )) {
							
							Order order = em.find(Order.class, dto.getOrderId());
									
							op.setOrder(order);
						}
					}
					
					em.flush();
					em.merge(op);
					tx.commit();
					
				} catch(Exception ex) {
					
					tx.rollback();
					throw new RuntimeException();
					
				} finally 	{
					em.close();
				}
				output = op;
			}
			
			else	{ //if( op == null ) 	{
				output = createOrderedProductfromDTO(dto);
			}
		}
		
		// create
		else { //if( dto.getId() == null  )	{
			
			output = createOrderedProductfromDTO(dto);
			
		}
		
		return output;
		
	}
	
	/*
	 *  DTO's OrderId and DTO's Product id should refer to an existing entity in the database.
	 */
	private static OrderedProduct createOrderedProductfromDTO( OrderedProductDTO opDTO )	{
		
		OrderedProduct newOrderedProduct = new OrderedProduct();
		
		EntityManager em = EntityManagerUtil.getEntityManager();
		
		EntityTransaction tx = em.getTransaction();
		
		Order theOrder = null;
		
		if(  opDTO.getOrderId() != null)	{
			
			theOrder = OrderCRUD.retreive( opDTO.getOrderId() );
			newOrderedProduct.setOrder(theOrder);
		}
		else	{
			throw new NullPointerException("When trying to save an OrderedProduct from a DTO, the orderId in the DTO is null (not present).");
		}
		
		Product product = null;
		
		if( opDTO.getProductId() != null)	{
			product = ProductCRUD.retreive(opDTO.getProductId() );
			newOrderedProduct.setProduct(product);
		}
		else	{
			throw new NullPointerException("When trying to save an OrderedProduct from a DTO, the productIdId in the DTO is null (not present).");
		}
		
		newOrderedProduct.setQuantity( opDTO.getQuantity() );
		
		newOrderedProduct = OrderedProductCRUD.create(newOrderedProduct);
		
		return newOrderedProduct;
	}
	
}
