package hu.unideb.szupercsapat.projekt.rest.service;

import hu.unideb.szupercsapat.projekt.jpa.ProductCRUD;
import hu.unideb.szupercsapat.projekt.model.products.Product;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import java.util.List;
import java.util.Set;

@Path("/")
public class ProductResource {

    /**
     * Termékek listázása kategória alapján.
     * Lista kezdete és hossza megadható.
     *
     * @param categoryId Kategória azonosítója.
     * @param start      Lista kezdete.
     * @param size       Lista hossza.
     * @return Termékek listája.
     */
    @GET
    @Path("/category/{category_id}/products")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public List<Product> getProductsByCategoryId(
    		@PathParam("category_id") Long categoryId, 
    		@QueryParam("start") @DefaultValue("0") int start, 
    		@QueryParam("size") @DefaultValue("20") int size) 	{
    	
    	List<Product> products =  ProductCRUD.retreiveByCategoryId(categoryId, start, size);
    	
    	if( products != null )	{
    		return products;
    	}
    	else	{
    		throw new WebApplicationException(404);
    	}
    }

    @GET
    @Path("category/{category_id}/subcategories/{subcategory_id}/products")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public List<Product> getProductsByCategoryIdAndSubCategoryId(
    		@PathParam("category_id") Long categoryId, 
    		@PathParam("subcategory_id") Long subCategoryId,
    		@QueryParam("start") @DefaultValue("0") int start, 
    		@QueryParam("size") @DefaultValue("20") int size)
    			{
    	List<Product> products = ProductCRUD.retreiveByCategoryIdAndSubCategoryId(categoryId, subCategoryId, start, size);
    	
    	if( products != null )	{
    		return products;
    	}
    	else	{
    		throw new WebApplicationException(404);
    	}
    	
    }
    
    @GET
    @Path("/products")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public List<Product> getProducts(
    		@QueryParam("start") @DefaultValue("0") int start, 
    		@QueryParam("size") @DefaultValue("20") int size)
    			{
    	List<Product> products = ProductCRUD.retreiveProductsOfAnyKind (start, size);
    	
    	if( products != null )	{
    		return products;
    	}
    	else	{
    		throw new WebApplicationException(404);
    	}
    }
    
    @GET
    @Path("/product/{product_id}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Product getProductById(@PathParam("product_id") Long productId )
    			{
    	Product product = ProductCRUD.retreive(productId);
    	
    	if( product != null )	{
    		return product;
    	}
    	else	{
    		throw new WebApplicationException("Product is not found...", 404);
    	}
    }
    
   
}
