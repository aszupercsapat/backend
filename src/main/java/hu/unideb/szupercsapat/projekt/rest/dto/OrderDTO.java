package hu.unideb.szupercsapat.projekt.rest.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.apache.commons.collections4.ListUtils;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import hu.unideb.szupercsapat.projekt.jpa.EntityManagerUtil;
import hu.unideb.szupercsapat.projekt.jpa.OrderCRUD;
import hu.unideb.szupercsapat.projekt.jpa.OrderedProductCRUD;
import hu.unideb.szupercsapat.projekt.jpa.UserCRUD;
import hu.unideb.szupercsapat.projekt.model.address.HunAddress;
import hu.unideb.szupercsapat.projekt.model.order.Order;
import hu.unideb.szupercsapat.projekt.model.order.OrderedProduct;
import hu.unideb.szupercsapat.projekt.model.user.User;

public class OrderDTO {

	private final Long id;
	private final HunAddress billingAddress;
	private final HunAddress shippingAddress;
	
	private final List<Long> orderedProductIds = new ArrayList<>();
	
	private final LocalDate orderDate;
	
	private final Long userId;

	@JsonCreator
	public OrderDTO(
			@JsonProperty("id")Long orderId, 
			@JsonProperty("billingAddress")HunAddress billingAddress, 
			@JsonProperty("shippingAddress")HunAddress shippingAddress, 
			@JsonProperty("orderedProductIds")List<Long> orderedProductIds,
			@JsonProperty("orderDate")LocalDate orderDate, 
			@JsonProperty("userId")Long userId) {
		super();
		this.id = orderId;
		this.billingAddress = billingAddress;
		this.shippingAddress = shippingAddress;
		if(orderedProductIds != null)	{
			this.orderedProductIds.addAll(orderedProductIds);
		}
		this.orderDate = orderDate;
		this.userId = userId;
	}

	public Long getId() {
		return id;
	}

	public HunAddress getBillingAddress() {
		return billingAddress;
	}

	public HunAddress getShippingAddress() {
		return shippingAddress;
	}

	public List<Long> getOrderedProductIds() {
		return orderedProductIds;
	}

	public LocalDate getOrderDate() {
		return orderDate;
	}
	
	public Long getUserId() {
		return userId;
	}

	public static OrderDTO OrderToDTO(Order order)	{
		
		List<Long> orderedProductIds 
				= new ArrayList<Long> (
					order.getOrderedProducts()
					.stream()
					.map( OrderedProduct::getId )
					.collect(Collectors.toSet()) 
				);
		
		return new OrderDTO(
				order.getId(), 
				order.getBillingAddress(), 
				order.getShippingAddress(), 
				orderedProductIds, 
				order.getOrderDate(),
				order.getUser().getId()
		);
	}
	
	/**
	 * For incoming OrderDTO-s. It will also update.
	 * @param dto
	 * @return
	 */
	public static Order dtoToOrder(OrderDTO dto)	{
		
		EntityManager em = EntityManagerUtil.getEntityManager();
		
		EntityTransaction tx = em.getTransaction();
		
		Order order = null;
		if( dto.getId() != null)	{
			order = OrderCRUD.retreive(dto.getId());
		}
		
		
		if( order != null)	{
			
			try	{
				tx.begin();
				
				em.refresh(order);
				
				if( !order.getBillingAddress().equals(dto.getBillingAddress()) )	{
					order.setBillingAddress(dto.getBillingAddress());
				}
				if( !order.getShippingAddress().equals(dto.getShippingAddress()) )	{
					order.setShippingAddress(dto.getShippingAddress());
				}
				if( !order.getUser().getId().equals(dto.getUserId()))	{
					order.setUser(em.find(User.class, dto.getUserId()));
				}
				
				Set<Long> ordersOrderedProductsIdsAsSet = 
							order.getOrderedProducts()
						.stream()
						.map( (op) -> op.getId() )
						.collect(Collectors.toSet());
				
				Set<Long> dtosOrderedProductIdsAsSet = new HashSet<Long>(dto.getOrderedProductIds());
				
				if( !ordersOrderedProductsIdsAsSet.equals(dtosOrderedProductIdsAsSet) )	{
					
					for( Long orderedProductId : dtosOrderedProductIdsAsSet)	{
						OrderedProduct op = em.find(OrderedProduct.class, orderedProductId);
						op.setOrder(order);
						em.merge(op);
					}
					
					List<Long> ordersOrderedProductsIdsAsList = new ArrayList<Long>( ordersOrderedProductsIdsAsSet );
					List<Long> dtosOrderedProductIdsAsList = new ArrayList<Long>( dtosOrderedProductIdsAsSet );
					
					List<Long> subtracted = ListUtils.subtract(ordersOrderedProductsIdsAsList, dtosOrderedProductIdsAsList);
					
					
					
					for( Long theId : subtracted)	{
						OrderedProduct op = em.find(OrderedProduct.class, theId);
						op.setOrder(null);
						em.merge(op);
					}
				}
				
				tx.commit();
			}
			catch(Exception ex) {
				
				tx.rollback();
				throw new RuntimeException();
			}
			finally	{
				em.close();
			}
			
		}
		
		// creating
		else	{	// if (order == null)
			
			Order newOrder = new Order();
			
			newOrder.setBillingAddress(dto.getBillingAddress()); 
			
			newOrder.setShippingAddress(dto.getShippingAddress());
			
			if( dto.getOrderDate() == null)	{
				
				newOrder.setOrderDate(LocalDate.now());
				
			}
			
			else	{
				
				newOrder.setOrderDate(dto.getOrderDate());
				
			}
			
			User user = UserCRUD.retreive( dto.getUserId() );
			
			if( user == null )	{
				throw new RuntimeException("Trying to create an Order, but the user is null");
			}
			
			newOrder.setUser(user);
			
			List<Long> orderedProductIds = dto.getOrderedProductIds();
			
			
			if(orderedProductIds != null)	{
				
				if(orderedProductIds.size() > 0)	{
					
					for( Long orderedProductId : orderedProductIds )	{
						
						OrderedProduct actualOrderedProduct = OrderedProductCRUD.retreive(orderedProductId);
						
						if( actualOrderedProduct != null)	{
							
							actualOrderedProduct.setOrder(newOrder);
							
						}	
					}
				}
			}
			
			try	{
				tx.begin();
				
				em.persist(newOrder);
				
				order = newOrder;
				
				em.flush();
				tx.commit();
				
				order = newOrder;
			}
			catch(Exception ex)	{
				tx.rollback();
				throw new RuntimeException(ex);
			}
			finally	{
				em.close();
			}
			
		}
		
		return order;
	}
	

}
