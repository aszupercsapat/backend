package hu.unideb.szupercsapat.projekt.rest.service;

import java.net.URI;

import javax.persistence.NoResultException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import hu.unideb.szupercsapat.projekt.jpa.OrderCRUD;
import hu.unideb.szupercsapat.projekt.jpa.OrderedProductCRUD;
import hu.unideb.szupercsapat.projekt.model.order.Order;
import hu.unideb.szupercsapat.projekt.model.order.OrderedProduct;
import hu.unideb.szupercsapat.projekt.rest.dto.OrderDTO;
import hu.unideb.szupercsapat.projekt.rest.dto.OrderedProductDTO;

@Path("/")
public class OrdersResource {

	
	@GET
	@Path("/orderedproduct/{op_id}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public OrderedProductDTO getOrderedProductById(@PathParam("op_id")Long opId )	{
		
		OrderedProduct orderedProduct = null;
		try	{
			orderedProduct = OrderedProductCRUD.retreive(opId);
			
			if(orderedProduct != null)	{
				return OrderedProductDTO.toOrderedProductDTO(orderedProduct) ;
			}
			else	{
				throw new WebApplicationException("OrderedProduct not found.", 404);
			}
			
		}		
		catch(Exception ex)	{
			throw new WebApplicationException(ex);
		}
		
	}
	
	@POST
	@Path("/orderedproduct/")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response postOrderedProduct( OrderedProductDTO opDTO)	{
		
		OrderedProduct op = null;
		try	{
			op = OrderedProductCRUD.retreive(opDTO.getId());
		}
		catch(NoResultException ex)	{
			op = null;
		}
		
		
		boolean isCreated = false;
		
		if( op == null)	{
			isCreated = true;
		}
		else	{
			isCreated = false;
		}
		
		try	{
			
			op = OrderedProductDTO.fromDTOToOrderedProduct(opDTO);
			
			if(isCreated)	{
				return Response.created(URI.create( "/orderedproduct/" + op.getId() )).status(201) .build();
			}
			else	{
				return Response.created(URI.create( "/orderedproduct/" + op.getId() )).status(200) .build();
			}
			
			
		} catch(Exception ex)	{
			throw new WebApplicationException("Something went wrong.", ex);
		}

	}
	
	
	
	@GET
	@Path("order/{order_id}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public OrderDTO getOrderById(@PathParam("order_id") Long orderId)	{
		
		Order order = OrderCRUD.retreive(orderId);
		
		if(order != null)	{
			return  OrderDTO.OrderToDTO(order);
		}
		else	{
			throw new WebApplicationException("Order not found.", 404);
		}
		
	}
	
	@POST
	@Path("/order/")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	//public Response postOrder(OrderDTO dto)	{
	public OrderDTO postOrder(OrderDTO dto)	{
		
		Order order = null;
		
		if( dto.getId() != null)	{
			try	{
				order = OrderCRUD.retreive(dto.getId());
			}
			catch(NoResultException ex)	{
				order = null;
			} catch(Exception ex)	{
				throw new WebApplicationException("Something went wrong.", ex);
			}
		}
		
		// or it will be created
		boolean isCreated = false;
		
		if( order == null)	{
			isCreated = true;
		}
		else	{
			isCreated = false;
		}
		
		try	{
			
			order = OrderDTO.dtoToOrder(dto);
			
			if(isCreated)	{
				//return Response.created(URI.create( "/order/" + order.getId() )).status(201) .build();
				return OrderDTO.OrderToDTO(order );
			}
			else	{
				//return Response.created(URI.create( "/order/" + order.getId() )).status(200) .build();
				return OrderDTO.OrderToDTO(order );
			}
			
			
		} catch(Exception ex)	{
			throw new WebApplicationException("Something went wrong.", ex);
		}

	}
}
