package hu.unideb.szupercsapat.projekt.rest;

import java.util.List;

import javax.persistence.EntityManager;

import hu.unideb.szupercsapat.projekt.jpa.CategoryCRUD;
import hu.unideb.szupercsapat.projekt.model.products.Category;
import hu.unideb.szupercsapat.projekt.model.products.SubCategory;

@Deprecated
public class CategorySubCategoryCircularDependencyFilterForJSON {
	
	private static EntityManager em = CategoryCRUD.getEm();

	public static List<Category> trimCategories( List<Category> detachedCategories)	{
		
		for(Category category : detachedCategories)	{
			
			for(SubCategory subCat : category.getSubCategories() )	{
				//em.detach(subCat);
				subCat.setCategories(null);
			}
		}
		
		return detachedCategories;
	}
}
