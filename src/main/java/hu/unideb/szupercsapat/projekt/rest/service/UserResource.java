package hu.unideb.szupercsapat.projekt.rest.service;

import java.io.InputStream;
import java.net.URI;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import hu.unideb.szupercsapat.projekt.jpa.UserCRUD;
import hu.unideb.szupercsapat.projekt.jpa.UserLoginException;
import hu.unideb.szupercsapat.projekt.model.user.User;

@Path("/")
public class UserResource {

	@GET
	@Path("/user/{user_id}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public User getUserResource(@PathParam("user_id") Long userId)	{
		
		User sendableUser = UserCRUD.retreive(userId);
		
		if(sendableUser != null)	{
			return sendableUser;
		}
		else	{
			Response response = Response.status(404).build();
			throw new WebApplicationException("User is not found with the given Id: " + userId, response);
			
		}
	}
	/*
	 * POST: response code 201 "Created" Location header must be the URI of the newly created resource
	 */
	@POST
	@Path("/user/")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response posrtUserResource(User user)	{
		if(user.getId() == null)	{
			try	{
				User savedUser = UserCRUD.create(user);
				return Response.created(URI.create( "/user/" + savedUser.getId() )).status(201) .build();
			} catch	(Exception ex){
				Response response = Response.status(500).build();
				throw new WebApplicationException( ex, response );
			}
		}
		else	{
			return  Response.status(406).build();
		}
		
	}
	
	@POST
	@Path("/userlogin/")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public User postUserLogin(User user)	{
		
		User outgoingUser = null;
		
		try	{
			
			outgoingUser = UserCRUD.userLogin(user);
			
		}catch (UserLoginException ulExp) {
			throw new WebApplicationException("Wrong username or password.", 401);
		}
		catch(Exception ex)	{
			throw new WebApplicationException(ex, 500);
		}
		
		return outgoingUser;
	}
	
}
