package hu.unideb.szupercsapat.projekt.rest.service;


import hu.unideb.szupercsapat.projekt.jpa.CategoryCRUD;
import hu.unideb.szupercsapat.projekt.model.products.Category;
import hu.unideb.szupercsapat.projekt.model.products.SubCategory;
import hu.unideb.szupercsapat.projekt.rest.CategorySubCategoryCircularDependencyFilterForJSON;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import java.util.List;
import java.util.Set;

@Path("/categories/")
public class CategoriesResource {
    /**
     * Ez listázza majd a kategóriákat JSONben
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public List<Category> getCategories() {
        //return CategorySubCategoryCircularDependencyFilterForJSON.trimCategories(CategoryCRUD.listCategoriesDetached());
    	return CategoryCRUD.listCategoriesDetached();
    }

    @GET
    @Path("{category_id}/subcategories")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Set<SubCategory> getSubCategoriesByCategoryId(@PathParam("category_id") Long categoryId) {
        return CategoryCRUD.listSubCategoriesByCategory(categoryId);
    }
    
    
}
