insert into Category (id, name) values (1, 'kutya'),(2, 'macska'),(3, 'h�rcs�g');
insert into SubCategory (id, name) values(1, 'eledel'),(2, 'j�t�k'),(3, 'ketrec');
INSERT INTO category_subcategory (category_id, subcategory_id) VALUES(1, 1),	(2, 1),	(1, 2),	(3, 1),	(3, 3),	(2, 2);
insert into Product (CATEGORY_ID, manufacturer, name, priceInHUF, shortDescription, SUBCATEGORY_ID, id) VALUES(1, 'PEDIGREE', 'Sz�raz kaja 400g', 500, 'vadh�sb�l', 1, 1),	(2, 'WHISKAS', 'Sz�raz kaja 200g', 300, 'marhah�sb�l', 1, 2),	(3, 'PURINA', 'Sz�raz kaja 400g', 500, 'j�', 1, 3),	(1, 'DOG TOYS INC.', 'Labda', 200, 'j�t�k a kedvencnek', 2, 4),	(3, 'PRISON INC.', 'Emeletes Ketrec', 4000, 'T�gas ketrec kis�llatoknak', 3, 5);
commit;