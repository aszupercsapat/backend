insert into Category (id, name) values (1, 'kutya'),(2, 'macska'),(3, 'h�rcs�g');
insert into SubCategory (id, name) values(1, 'eledel'),(2, 'j�t�k'),(3, 'ketrec');
INSERT INTO category_subcategory (category_id, subcategory_id) VALUES(1, 1),	(2, 1),	(1, 2),	(3, 1),	(3, 3),	(2, 2);
insert into Product (CATEGORY_ID, manufacturer, name, priceInHUF, shortDescription, SUBCATEGORY_ID, id) VALUES(1, 'PEDIGREE', 'Sz�raz kaja 400g', 500, 'vadh�sb�l', 1, 1),	(2, 'WHISKAS', 'Sz�raz kaja 200g', 300, 'marhah�sb�l', 1, 2),	(3, 'PURINA', 'Sz�raz kaja 400g', 500, 'j�', 1, 3),	(1, 'DOG TOYS INC.', 'Labda', 200, 'j�t�k a kedvencnek', 2, 4),	(3, 'PRISON INC.', 'Emeletes Ketrec', 4000, 'T�gas ketrec kis�llatoknak', 3, 5);
/*
insert into users(id, emailaddress, hashedpassword, rights, city, countryenum, door, floornumber, nameofpublicplace, postalcode, streetnumber, typeofpublicplace, firstname, lastname, phonenumber)
values...

*/
/*
 * -- Actual parameter values may differ, what you see is a default string representation of values
INSERT INTO public.orders (id,billing_city,billing_country_enum,billing_door,billing_floor_number,billing_name_of_public_place,billing_postal_code,billing_street_number,billing_type_of_public_place,orderdate,city,countryenum,door,floornumber,nameofpublicplace,postalcode,streetnumber,typeofpublicplace,user_id)
VALUES (101,'Debrecen','HUNGARY','5',2,'Piac',4030,'136','utca','2018-11-25','Debrecen','HUNGARY','30',8,'Vincell�r',4031,'160','utca',101) ;
 */

/*
 * -- Actual parameter values may differ, what you see is a default string representation of values
INSERT INTO public.users (id,emailaddress,hashedpassword,rights,city,countryenum,door,floornumber,nameofpublicplace,postalcode,streetnumber,typeofpublicplace,firstname,lastname,phonenumber)
VALUES (101,'valaki2@hotmail.com','asd2342rwe','CUSTOMER','Debrecen','HUNGARY','4A',2,'Jerik�',4029,'5','utca','Elek','Mekk','+363012345678') ;

 */

/*
 * -- Actual parameter values may differ, what you see is a default string representation of values
INSERT INTO public.orderedproduct (id,quantity,order_id,product_id)
VALUES (1,4,101,1) ;

 */
commit;