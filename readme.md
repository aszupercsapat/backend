Először is kell Applikáció szerver.

Én Wildfly 14.0.1 Finalt használtam:
http://wildfly.org/downloads/

Ezt be is kell konfigurálni, kell egy Usert készíteni. Hozzá kell adni egy global module-t hogy ne adjon jibát a dom4j, és engedélyezni kell a CORS-t. Az egészet ledokumentáltam itt:
https://bitbucket.org/aszupercsapat/documents/src/master/widlfly.txt

Kell egy postgreSQL. Két lehetőség van, vagy telepítessz egy postgreSQL-t csinálsz egy adatbázist "webshoppets" néven, vagy van egy cloudos megoldás, ami működik, de sokkal lassabb.
A Cloudos: Van egy "persistence_postgre_cloud.xml" nevű file amit át kell nevezni persistence.xml-re az src/java/webapp/WEB-INF/classes/META-INF mappában. Akkor én teszek fel kezdő adatot, és ha nincs drop and create-akkor az xml-ben az ott is fog maradni.
Ez azért fontos mert a menü is az adatbázisból veszi ki a kategóriákat ténylegesen, és nem lenne túl látványos, ha nincs egy termék se, se pedig kategória. Akkor csak login lenne.
FONTOS: alapból nincs persistence.xml, de van átnevezett persistence.xml  a megfelelő mappában, vissza kell nevezni " persistence.xml " -re.

A tesztekhez is kell adatbázis ezért javaslom a buildeléshez az
"mvn clean package -DskipTests" -et.